﻿using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefPushback")]
public class RefPushBack : RefVariable<PushBack>{}