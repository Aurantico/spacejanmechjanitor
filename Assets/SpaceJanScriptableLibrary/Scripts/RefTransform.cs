﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefTransform")]
public class RefTransform : RefVariable<Transform>
{
}
