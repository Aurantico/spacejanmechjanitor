﻿using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefUIAnimationsManager")]
public class RefUserInterfaceAnimationsManager : RefVariable<UIAnimationsManager>
{
}
