﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefMeshRenderer")]
public class RefMeshRenderer : RefVariable<MeshRenderer> {}
