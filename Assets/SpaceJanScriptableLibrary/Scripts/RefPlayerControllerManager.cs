﻿using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefPlayerControllerManager")]
public class RefPlayerControllerManager : RefVariable<PlayerControllerManager>
{
}
