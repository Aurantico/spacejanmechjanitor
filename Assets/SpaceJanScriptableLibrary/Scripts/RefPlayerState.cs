﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefPlayerState")]
public class RefPlayerState : RefVariable<PlayerState> {}
