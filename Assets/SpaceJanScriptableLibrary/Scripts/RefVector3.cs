﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "SpaceJanRefVariables/RefVector3")]
public class RefVector3 : RefVariable<Vector3> {}
