﻿using UnityEngine;

public class HoverDrone : AIBot
{
    [Header("Player Position")]
    [SerializeField]
    private RefTransform _Target;

    [Header("Fire Rate (in seconds)")]
    [SerializeField]
    private float _RefireTime = 2.0f;
    private float _WaitingTime = 0f;

    [SerializeField]
    private RefBool _IsInCombatBool;

    private bool _CannotShoot;

    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _PlayHoveringAudio;
    // [SerializeField]
    // private AK.Wwise.Event _FireAudio;
    // [SerializeField]
    // private AK.Wwise.Event _DeathAudio;

    private ShootingMissile _ShootingMissile;

    protected override void OnEnable()
    {
        base.OnEnable();
        _QTEBehaviour.Value.OnSuccess += SetCannotShoot;
        _QTEBehaviour.Value.OnFail += SetCanShoot;
        // _QTEBehaviour.Value.OnFail += UpdateWaypointTarget;
        // _QTEBehaviour.Value.OnFail += EnableMovement;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        _QTEBehaviour.Value.OnSuccess -= SetCannotShoot;
        _QTEBehaviour.Value.OnFail -= SetCanShoot;
        // _QTEBehaviour.Value.OnFail -= UpdateWaypointTarget;
        // _QTEBehaviour.Value.OnFail -= EnableMovement;
    }
    private void Start()
    {
        _ShootingMissile = GetComponentInChildren<ShootingMissile>();
        _Waypoints = GetComponent<Waypoints>();

        // //Debug.Log("HOVERING NEEDS TO PLAY"); 
        // _PlayHoveringAudio.Post(this.gameObject);
    }

    private void OnDestroy()
    {
        // //Debug.Log("HOVERING STILL NEEDS TO BE STOPPED"); 
        // _DeathAudio.Post(this.gameObject);
    }

    protected override void Update()
    {
        base.Update();
        _WaitingTime += Time.deltaTime;
    }

    private void OnTriggerStay(Collider other)
    {
        PlayerControllerManager player = other.gameObject.GetComponent<PlayerControllerManager>();

        // EnableMovement();
        LookAtPlayer();
        if (player != null)
        {
            if (_ShootingMissile.ShootingPosition != null && _CannotShoot == false)
            {
                Shoot();
            }
        }
    }

    public override void EnableMovement()
    {
        _Waypoints.NotifyCanMove();
    }
    public override void UpdateWaypointTarget()
    {
        _Waypoints.UpdateTarget();
    }

    private void LookAtPlayer()
    {
        transform.LookAt(_Target.Value.position, Vector3.up);
    }

    private void SetCannotShoot()
    {
        _CannotShoot = true;
    }

    private void SetCanShoot()
    {
        _CannotShoot = false;
    }

    private void Shoot()
    {
        // if (_IsInCombatBool.Value == true)
        // {
        //     return;
        // }

        if (_WaitingTime >= _RefireTime)
        {

            // _FireAudio.Post(this.gameObject);
            _ShootingMissile.Shoot();
            _WaitingTime = 0f;
        }
    }
}