﻿using System;
using UnityEngine;

public class ShootingMissile : MonoBehaviour
{
    [Header("Missile Prefab")]
    [SerializeField]
    private GameObject _HomingMissile;

    [Header("Instansiate Position")]
    [SerializeField]
    private Transform _ShootingPosition;
    
    

    public Transform ShootingPosition
    {
        get
        {
            return _ShootingPosition;
        } 
    }

    public void Shoot()
    {
        Instantiate(_HomingMissile, _ShootingPosition.position, Quaternion.identity);
    }

  
    
}
