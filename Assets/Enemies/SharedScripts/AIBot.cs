﻿using System;
using UnityEngine;

public class AIBot : MonoBehaviour
{
    [Header("QTE Behaviour")]
    [SerializeField]
    protected RefQTEBehaviour _QTEBehaviour;

    [Header("Particle System")]
    [SerializeField]
    protected ParticleSystem _Explosion;
    
    [Header("Enemy Death Settings")]
    [SerializeField,Range(0.0f, 20.0f)]
    private float _DeathTime;

    private bool _IsAlive;

    protected Waypoints _Waypoints;

    [Header("Bot ID")]
    [SerializeField]
    protected BotType _BotType;

    private RawTimer _DeathTimer = new RawTimer();

    protected virtual void Awake()
    {
        _IsAlive = true;
    }

    protected virtual void OnEnable()
    {
        _DeathTimer.NotifyOnTimerEnd += SetFinalDeathParameters;
    }

    protected virtual void Update()
    {
        _DeathTimer.TimerTick(Time.deltaTime);
        CheckDeath();
    }
    
    protected virtual void OnDisable()
    {
        // enemy death if QTE success
        _QTEBehaviour.Value.OnSuccess -= StartEnemyDeathSequence;
    }

    public virtual void CheckDeath()
    {
        if (_IsAlive == true)
        {
            return;
        }
        
        transform.parent.gameObject.SetActive(false);
    }
    
    public BotType GetBotType()
    {
        return _BotType;
    }
    public virtual void EnableMovement()
    {

    }
    public virtual void UpdateWaypointTarget()
    {

    }
    
    public void StartEnemyDeathSequence()
    {
        Debug.Log($"StartEnemyDeathSequence {_DeathTime}", this);
        _DeathTimer.StartTimer(_DeathTime);
    }

    public void SetFinalDeathParameters()
    {
        Debug.Log("SetFinalDeathParameters", this);
        Instantiate(_Explosion, transform.position, Quaternion.identity);
        _IsAlive = false;
    }
}

public enum BotType { Hover, Shield };
