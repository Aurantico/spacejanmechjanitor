﻿using UnityEngine;
using UnityEngine.UI;

public class QTETimeSlider : MonoBehaviour
{
    [Header("SO References")]
    [SerializeField]
    private RefBool _IsQTEActive;

    private QTEEvent _QTEEvent;
    private QTEBehaviour _QTEBehaviour;
    private Slider _TimeSlider;

    private void Awake()
    {
        GetComponents();
    }

    private void OnEnable()
    {
        SetupTimer();
        _QTEEvent.SlowDown();
    }

    private void OnDisable()
    {
        _QTEEvent.SpeedUp();
    }

    private void Update()
    {
        if (_IsQTEActive.Value == false) return;

        UpdateTimer();
    }

    private void SetupTimer()
    {
        _TimeSlider.maxValue = _QTEBehaviour.GetQTEDuration();
        _TimeSlider.minValue = 0f;

        _TimeSlider.value = _QTEBehaviour.GetTimer().Time;
    }

    private void UpdateTimer()
    {
        _TimeSlider.value = _QTEBehaviour.GetTimer().Time;
    }

    private void GetComponents()
    {
        _QTEEvent = GetComponentInParent<QTEEvent>();
        _QTEBehaviour = GetComponentInParent<QTEBehaviour>();
        _TimeSlider = GetComponent<Slider>();
    }
}
