using GameSavvy.OpenUnityAttributes;
using UnityEngine;
using ObjectPooling;
using System.Collections.Generic;

public class QTEButtonDisplay : MonoBehaviour
{
    [Header("Button Attributes")]
    [SerializeField]
    private QTEButton _ButtonPrefab;

    [SerializeField, ReorderableList]
    private ButtonType[] _ButtonSprites;

    [Space(10), SerializeField, InlineButton("GetQTEBehaviour")]
    private QTEBehaviour _QTEBehaviour;

    private Queue<QTEButton> _ButtonQueue = new Queue<QTEButton>();

    #region Unity Methods
    private void OnEnable()
    {
        EnableQTEButtons();
    }

    private void OnDisable()
    {
        DisableQTEButtons();
    }
    #endregion

    #region UI Display Methods
    private void EnableQTEButtons()
    {
        foreach (var inputType in _QTEBehaviour.GetQTEQueue())
        {
            // Create new button as a poolable object.
            var newButton = PoolManager.GetNext(_ButtonPrefab, transform.position, Quaternion.identity);

            // Enqueue the new button as a QTEButton.
            _ButtonQueue.Enqueue(newButton as QTEButton);

            // Set this object as the parent.
            newButton.transform.SetParent(transform);
            newButton.transform.SetAsLastSibling();

            // Reset scale of the object to one.
            newButton.transform.localScale = Vector3.one;

            SetButtonImage(newButton as QTEButton, inputType);
        }
    }

    private void SetButtonImage(QTEButton button, InputType type)
    {
        foreach (var buttonType in _ButtonSprites)
        {
            if (type == buttonType.MappedInputType)
            {
                button.SetSprite(buttonType.Sprite);
            }
        }
    }

    private void DisableQTEButtons()
    {
        _ButtonQueue.Clear();
        
        foreach (Transform button in transform)
        {
            button.gameObject.SetActive(false);
        }
    }

    public void CorrectInput()
    {
        if (_ButtonQueue.Peek() == null) return;

        _ButtonQueue.Peek().Succeeded();
        _ButtonQueue.Dequeue();
    }

    public void IncorrectInput()
    {
        _ButtonQueue.Clear();
    }
    #endregion

    #region Initial Methods
    private void GetQTEBehaviour()
    {
        _QTEBehaviour = GetComponentInParent<QTEBehaviour>();
    }
    #endregion
}
