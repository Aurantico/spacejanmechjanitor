﻿using System;
using System.Collections.Generic;
using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class QTEBehaviour : MonoBehaviour
{
    [Header("SO References")]
    [SerializeField]
    private RefBool _IsQTEActive;

    [Header("QTE Attributes")]
    [SerializeField]
    private int _QTELength = 3;
    [SerializeField]
    private int _MaxQTELength = 6;
    [SerializeField]
    private int _MinQTELength = 3;

    [Header("QTE Time")]
    [SerializeField]
    private float _QTEDuration = 4f;
    [SerializeField]
    private float _TimeToAddOnSuccess = 0.5f;

    [Space(5), SerializeField, InlineButton("GetThisQTEButtonDisplay")]
    private QTEButtonDisplay _QTEButtonDisplay;

    [Space(5), SerializeField, InlineButton("GetThisQTETimeSlider")]
    private QTETimeSlider _QTETimeSlider;

    public event Action OnNotifyQTEStart;
    public event Action OnNotifyQTEEnd;

    public event Action<InputType> OnInputSuccess;
    public event Action OnFail;
    public event Action OnSuccess;

    private Queue<InputType> _QTEInputs = new Queue<InputType>();
    private InputTypeGenerator _ITG = new InputTypeGenerator();
    private Timer _Timer = new Timer();
    private int _ScoreCounter = 0;
    private int _QTEComboCount = 0;

    // [Header("Wwise")]
    // [SerializeField]
    // private AK.Wwise.Event _ButtonPressAudio;
    // [SerializeField]
    // private AK.Wwise.Event _SuccessQTEAudio;
    // [SerializeField]
    // private AK.Wwise.Event _FailQTEAudio;

    #region Unity Methods
    private void OnEnable()
    {
        StartQTE(CalculateQTEButtonLength(), CalculateQTETimeDuration());
        _Timer.OnTimerEnd += TimerIsUp;
    }

    private void OnDisable()
    {
        _Timer.OnTimerEnd -= TimerIsUp;
    }

    private void Update()
    {
        if (_IsQTEActive.Value == false) return;

        _Timer.Tick(Time.unscaledDeltaTime);
    }
    #endregion

    #region Timer Methods
    private void TimerIsUp()
    {
        OnFailedQTEEvent();
    }
    #endregion

    #region QTE Methods
    public void EnterButtonInput(InputType button)
    {
        if (_QTEInputs.Count == 0 || _IsQTEActive.Value == false) return;

        if (_QTEInputs.Peek() == button)
        {
            OnSuccessfulButtonPress(button);

            if (_QTEInputs.Count == 0)
            {
                OnSuccessfulQTEEvent();
            }
        }
        else
        {
            OnFailedQTEEvent();
        }
    }

    private void StartQTE(int size, float duration)
    {
        for (int i = 0; i < size; i++)
        {
            _QTEInputs.Enqueue(_ITG.GetRandom());
        }

        _Timer.StartTimer(duration);
        _ScoreCounter = 0;
        _QTETimeSlider.gameObject.SetActive(true);
        _QTEButtonDisplay.gameObject.SetActive(true);
        OnNotifyQTEStart?.Invoke();
    }

    private void EndQTE()
    {
        if (_IsQTEActive.Value == false) return;

        _IsQTEActive.Value = false;
        _QTEButtonDisplay.IncorrectInput();
        _QTEInputs.Clear();
        _Timer.StopTimer();
        _QTETimeSlider.gameObject.SetActive(false);
        _QTEButtonDisplay.gameObject.SetActive(false);
        OnNotifyQTEEnd?.Invoke();
        gameObject.SetActive(false);
    }

    private void OnSuccessfulButtonPress(InputType button)
    {
        // // WWISE: button press audio
        // _ButtonPressAudio.Post(this.gameObject);

        _ScoreCounter++;
        _QTEButtonDisplay.CorrectInput();
        _QTEInputs.Dequeue();
        OnInputSuccess?.Invoke(button);
    }

    private void OnSuccessfulQTEEvent()
    {
        // // WWISE: success audio
        // _SuccessQTEAudio.Post(this.gameObject);

        _QTEComboCount++;
        OnSuccess?.Invoke();
        EndQTE();
    }

    private void OnFailedQTEEvent()
    {
        // // WWISE: failure audio
        // _FailQTEAudio.Post(this.gameObject);

        OnFail?.Invoke();
        ResetButtonCount();
        EndQTE();
    }

    private int CalculateQTEButtonLength()
    {
        int numberOfButtons = 0;
        numberOfButtons = _QTELength + _QTEComboCount;
        numberOfButtons = Mathf.Clamp(numberOfButtons, _MinQTELength, _MaxQTELength);
        return numberOfButtons;
    }

    private float CalculateQTETimeDuration()
    {
        float timeDuration = 0;
        timeDuration = _QTEDuration + (_TimeToAddOnSuccess * _QTEComboCount);
        return timeDuration;
    }

    private void ResetButtonCount()
    {
        _QTEComboCount = 0;
    }
    #endregion

    #region Initializing Methods
    private void GetThisQTEButtonDisplay()
    {
        _QTEButtonDisplay = GetComponentInChildren<QTEButtonDisplay>(true);
    }

    private void GetThisQTETimeSlider()
    {
        _QTETimeSlider = GetComponentInChildren<QTETimeSlider>(true);
    }
    #endregion

    #region Public Methods and Variables
    public Queue<InputType> GetQTEQueue()
    {
        return _QTEInputs;
    }

    public Timer GetTimer()
    {
        return _Timer;
    }

    public float GetQTEDuration()
    {
        return CalculateQTETimeDuration();
    }
    #endregion
}
