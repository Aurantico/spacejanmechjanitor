﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class SetTextTo : MonoBehaviour
{
    private Text _Text;

    private void Awake()
    {
        _Text = GetComponent<Text>();
    }

    public void SetText(string str)
    {
        _Text.text = str;
    }

    public void SetText(int i)
    {
        _Text.text = i.ToString();
    }
}
