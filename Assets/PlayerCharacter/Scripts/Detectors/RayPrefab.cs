﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayPrefab : MonoBehaviour
{
    [SerializeField]
    private LayerMask _LayerMask;
    public Ray Ray { get; private set; }
    private Ray _Ray;
    public RaycastHit RayCastHit
    {
        get { return _RayCastHit;}
    }
    private RaycastHit _RayCastHit;

    [SerializeField]
    private bool _IsARightRay;
    public bool IsARightRay
    {
        get { return _IsARightRay; }
    }

    [SerializeField]
    private float _MaxDistance;

    public float MaxDistance => _MaxDistance;
    
    private void Update()
    {
        Ray = new Ray(transform.position, transform.forward);
    }

    public bool RayHit()
    {
        return Physics.Raycast(Ray, out _RayCastHit ,_MaxDistance, _LayerMask);
    }
}
