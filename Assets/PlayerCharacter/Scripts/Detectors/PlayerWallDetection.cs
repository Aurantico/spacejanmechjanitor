﻿using UnityEngine;

public class PlayerWallDetection : PlayerStateDetector
{
    [SerializeField]
    private RefBool _CanWallRideBool;

    private Ray _LeftRay;
    private RaycastHit _LeftRayHit;

    private Ray _RightRay;
    private RaycastHit _RightRayHit;
    [SerializeField]
    private RefBool _IsWallRidingRightBool;

    [SerializeField]
    private RefVector3 _VectorAlongRunnableWall;
    [SerializeField]
    private RefVector3 _HorizontalDirectionToNormal;
    [SerializeField]
    private RefRayPrefab _ShortestRayPrefab;
    [SerializeField]
    private RefRayGameObject _DetectedWallObject;

    protected override void Update()
    {
        ValidateWallDetection();
    }

    private void ValidateWallDetection()
    {
        if (_ShortestRayPrefab.Value == null)
        {
            return;
        }
        
        if (_ShortestRayPrefab.Value.RayHit() == true)
        {
            if (_ShortestRayPrefab.Value.RayCastHit.collider.gameObject == _DetectedWallObject.Value)
            {
                _CanWallRideBool.Value = false;
            }
            else
            {
                //do as normal
                if (Vector3.Dot(Vector3.up, _ShortestRayPrefab.Value.RayCastHit.normal) == 0.0f)
                {
                    if (_ShortestRayPrefab.Value.IsARightRay == true)
                    {
                        _VectorAlongRunnableWall.Value = (Vector3.Cross(Vector3.up, _ShortestRayPrefab.Value.RayCastHit.normal));
                    }
                    else
                    {
                        _VectorAlongRunnableWall.Value = -(Vector3.Cross(Vector3.up, _ShortestRayPrefab.Value.RayCastHit.normal));
                    }
                    //canwallride is true
                    _CanWallRideBool.Value = true;
                    _IsWallRidingRightBool.Value = _ShortestRayPrefab.Value.IsARightRay;
                    _HorizontalDirectionToNormal.Value = -_ShortestRayPrefab.Value.RayCastHit.normal;
                }
                else
                {
                    //canwallride is false
                    _CanWallRideBool.Value = false;
                }
            }
        }
        else
        {
            _CanWallRideBool.Value = false;
        }
    }
}
