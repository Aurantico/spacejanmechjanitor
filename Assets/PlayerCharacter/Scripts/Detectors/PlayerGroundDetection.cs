﻿using UnityEngine;

public class PlayerGroundDetection : PlayerStateDetector
{
    [SerializeField, Range(0.0f, 50.0f)]
    private float _SphereCastRadius;
    [SerializeField]
    private bool _DrawMyGizmos;
    [SerializeField]
    private RefBool _CanDashBool;

    [SerializeField]
    private Transform _SpherePosition;

    private Vector3 _Center;

    private Color _CurrentColor;
    private Collider [] _AllColliders;
    
    protected override void Update()
    {
        BetterGroundDetection();
    }
    

    private void BetterGroundDetection()
    {
        Ray pointingDownRay = new Ray(_PlayerCapsule.bounds.center, Vector3.down);

        //_AllColliders = Physics.SphereCastAll(pointingDownRay, _SphereCastRadius, _DetectionOffset, _DetectorLayerMask);
        //_AllColliders = Physics.BoxCastAll(_PlayerCapsule.bounds.center,_PlayerCapsule.bounds.extents * _SphereCastRadius,Vector3.down * _DetectionOffset,Quaternion.identity, _DetectorLayerMask);
        _AllColliders = Physics.OverlapSphere(_SpherePosition.position, _SphereCastRadius, _DetectorLayerMask);
        
        if ( _AllColliders.Length > 0)//not in the air
        {
            _CurrentColor = Color.green;
            _IsInAirBool.Value = false;
            _AllColliders = null;
        }
        else
        {
            _CurrentColor = Color.red;
            _IsInAirBool.Value = true;
        }
    }

    private void OnDrawGizmos()
    {
        if (_DrawMyGizmos)
        {
            Gizmos.color = _CurrentColor;
            Gizmos.DrawSphere(_SpherePosition.position, _SphereCastRadius); 
        }
    }
}
