﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimationInterface : MonoBehaviour
{
    [SerializeField]
    private RefFloat _PlayerGroundSpeedMagnitude;
    [SerializeField]
    private RefBool _IsInAirBool;
    [SerializeField]
    private RefBool _IsSprintingBool;
    [SerializeField]
    private RefBool _IsOnGrindRailBool;
    [SerializeField]
    private RefBool _IsWallRidingBool;
    [SerializeField]
    private RefBool _IsWallRidingRightBool;
    [SerializeField]
    private RefBool _IsInCombat;
    [SerializeField]
    private RefInteger _PlayerJumpCounter;
    [SerializeField]
    private RefFloat _PlayerVerticalVelocity;
    [SerializeField]
    private RefAnimator _PedroAnimator;

    private Animator _Animator;

    private void Awake()
    {
        _Animator = GetComponent<Animator>();
        _PedroAnimator.Value = _Animator;
    }

    private void Update()
    {
        GroundAnimationController();
        AerialAnimationController();
        GrindAnimationController();
        WallRideAnimationController();
    }

    private void GroundAnimationController()
    {
        _Animator.SetFloat("PlayerGroundSpeed", _PlayerGroundSpeedMagnitude.Value);
        
        // _Animator.SetBool("IsSprinting", _IsSprintingBool.Value);
    }

    private void AerialAnimationController()
    {
        _Animator.SetBool("IsPlayerInAir", _IsInAirBool.Value);
        _Animator.SetFloat("PlayerVerticalVelocity",_PlayerVerticalVelocity.Value);
        // _Animator.SetInteger("NumberOfJumps", _PlayerJumpCounter.Value);
    }

    private void GrindAnimationController()
    {
        _Animator.SetBool("IsPlayerInGrind", _IsOnGrindRailBool.Value);
    }

    private void CombatAnimationController()
    {
        // _Animator.SetBool("IsInCombat", _IsInCombat.Value);
    }

    private void WallRideAnimationController()
    {
        _Animator.SetBool("IsPlayerWallRiding", _IsWallRidingBool.Value);
        _Animator.SetBool("IsPlayerWallRidingRight",_IsWallRidingRightBool.Value);
    }
}
