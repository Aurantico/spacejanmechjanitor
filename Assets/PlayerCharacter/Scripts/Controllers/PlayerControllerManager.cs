﻿using System;
using UnityEngine;

public class PlayerControllerManager : MonoBehaviour
{
    //the controller manager tells which controller to be enabled and disables the rest.
    private PlayerController _CurrentController;

    [SerializeField]
    private RefBool _IsInAirBool;

    [SerializeField]
    private RefBool _IsInCombatBool;

    [SerializeField]
    private RefBool _IsOnGrindRailBool;

    [SerializeField]
    private RefBool _IsWallRidingBool;

    [SerializeField]
    private RefPlayerControllerManager _PlayerControllerManagerAsset;

    private PlayerController[] _PlayerControllers;

    [SerializeField]
    private RefTransform _PlayerPosition;

    private PlayerState _PlayerState;
    [SerializeField]
    private RefFloat _CurrentScore;

    [SerializeField]
    private RefFloat _NewScore;

    private Rigidbody _Rigidbody;

    [SerializeField]
    private RefUserInterfaceManager _UIManager;

    private RawTimer _ScoreTimer = new RawTimer();

    public event Action NotifyOnScoreAdded;

    public PlayerState LastPlayerState { get; private set; }

    private void Awake()
    {
        _PlayerControllers = GetComponents<PlayerController>();
        _PlayerControllerManagerAsset.Value = this;
        _IsOnGrindRailBool.Value = false;
        _NewScore.Value = 0.0f;
        _CurrentScore.Value = 0.0f;
        _PlayerPosition.Value = transform;
        _Rigidbody = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        if (_UIManager.Value == null)
        {
            return;
        }

        _UIManager.Value.OnPause += DisableLastInputMap;
        _UIManager.Value.OnUnpause += EnableLastInputMap;
    }

    private void OnDisable()
    {
        _IsInCombatBool.Value = false;
        if (_UIManager.Value == null)
        {
            return;
        }

        _UIManager.Value.OnPause -= DisableLastInputMap;
        _UIManager.Value.OnUnpause -= EnableLastInputMap;
    }

    private void Update()
    {
        _PlayerPosition.Value = transform;
        StateChangeDetection();

        // NEVER... EVER... IN YOUR LIFE!!! DO THIS!
        // Use a proper State Design pattern, this was eplained and taught to you on Unity_2
        // TODO: UPDATE PLEASE;
        switch (_PlayerState)
        {
            case PlayerState.GROUNDED:
                //switch to ground controller
                _CurrentController = _PlayerControllers[0];
                _CurrentController.enabled = true;
                LastPlayerState = PlayerState.GROUNDED;
                DisableControllers(_CurrentController);
                //Disable all others
                break;
            case PlayerState.AERIAL:
                //switch to ground controller
                _CurrentController = _PlayerControllers[1];
                _CurrentController.enabled = true;
                LastPlayerState = PlayerState.AERIAL;
                DisableControllers(_CurrentController);
                //Disable all others
                break;
            case PlayerState.WALLRIDE:
                //switch to ground controller
                _CurrentController = _PlayerControllers[2];
                _CurrentController.enabled = true;
                DisableControllers(_CurrentController);
                //Disable all others
                break;
            case PlayerState.GRIND:
                //switch to ground controller
                _CurrentController = _PlayerControllers[3];
                _CurrentController.enabled = true;
                DisableControllers(_CurrentController);
                //Disable all others
                break;
            case PlayerState.COMBAT:
                //switch to ground controller
                _CurrentController = _PlayerControllers[4];
                _CurrentController.enabled = true;
                DisableControllers(_CurrentController);
                //Disable all others
                break;
        }

        CountScoreUpSequence();
    }

    private void DisableControllers(PlayerController currentController)
    {
        for (var i = 0; i < _PlayerControllers.Length; i++)
            if (_PlayerControllers[i] != currentController)
            {
                _PlayerControllers[i].enabled = false;
            }
    }

    private void StateChangeDetection()
    {
        var playerIsInAir = _IsInAirBool.Value && _IsWallRidingBool.Value == false;
        var playerIsWallRiding = _IsInAirBool.Value && _IsWallRidingBool.Value;
        var playerIsGrounded = _IsInAirBool.Value == false && _IsWallRidingBool.Value == false;
        var playerIsInCombat = (playerIsGrounded || playerIsInAir) && _IsInCombatBool.Value == true;
        var playerIsInGrind = (playerIsGrounded || playerIsInAir) && _IsOnGrindRailBool.Value == true;

        if (playerIsGrounded) _PlayerState = PlayerState.GROUNDED;
        if (playerIsInAir) _PlayerState = PlayerState.AERIAL;
        if (playerIsWallRiding) _PlayerState = PlayerState.WALLRIDE;

        if (playerIsInCombat) _PlayerState = PlayerState.COMBAT;

        if (playerIsInGrind) _PlayerState = PlayerState.GRIND;
    }

    public void CombatStateSwitch(PlayerState currentState)
    {
        _PlayerState = currentState;
    }


    public void InvokeAddToScore()
    {
        NotifyOnScoreAdded?.Invoke();
        Debug.Log($"Working");
    }

    private void CountScoreUpSequence()
    {
        if (_CurrentScore.Value != _NewScore.Value)
        {
            _CurrentScore.Value = Mathf.Lerp(_CurrentScore.Value, _NewScore.Value, Time.deltaTime * 1.0f);
        }
    }

    private void DisableLastInputMap()
    {
        _CurrentController.MyInputMap.Disable();
    }
    private void EnableLastInputMap()
    {
        _CurrentController.MyInputMap.Enable();
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 120, 20), $"Score: {Mathf.Round(_CurrentScore.Value)}", GUI.skin.button);
        GUI.Label(new Rect(1000, 10, 120, 20), $"Velocity: {Mathf.Round(_Rigidbody.velocity.magnitude)}", GUI.skin.button);
    }

}
