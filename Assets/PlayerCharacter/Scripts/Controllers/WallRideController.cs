﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class WallRideController : PlayerController, IWallRide, IJump
{
    [SerializeField]
    private RefBool _CanWallRideBool;

    [SerializeField]
    private RefVector3 _VectorAlongRunnableWall;
    [SerializeField]
    private RefVector3 _HorizontalDirectionToNormal;
    [SerializeField]
    private RefBool _IsWallRidingBool;
    [SerializeField]
    private RefRayGameObject _DetectedWallObject;
    [SerializeField]
    private RefRayPrefab _ShortestRayPrefab;
    [SerializeField, Range(0.0f, 600.0f)]
    private float _WallRideSpeedMagnitude;

    [SerializeField, Range(0.0f, 60.0f)]
    private float _PushToWallForceMagnitude;
    [SerializeField, Range(0,10)]
    private int _NumberOfJumps;
    [SerializeField, Range(0,10)]
    private int _NumberOfDashes;
    [SerializeField]
    private float _SnapSpeed;
    private bool _IsWallRidePressed;


    private void Update()
    {
        ValidateWallRideBool();
    }

    private void FixedUpdate()
    {
        WallRide();
    }

    private void OnEnable()
    {
        _InputActionAsset.WallRideInputMap.Enable();
        SetMyInputMap(_InputActionAsset.WallRideInputMap);
        _Rigidbody.velocity = new Vector3(_Rigidbody.velocity.x, 0.0f, _Rigidbody.velocity.z);
        SubscribeInputActions();
        _PlayerJumpCounter.Value = _NumberOfJumps;
        _PlayerDashCounter.Value = _NumberOfDashes;
    }

    private void OnDisable()
    {
        _IsWallRidePressed = false;
        _IsWallRidingBool.Value = false;
        _InputActionAsset.WallRideInputMap.Disable();
        UnsubscribeInputActions();
    }
    
    private void OnWallRideJumpPerformed(InputAction.CallbackContext context)
    {
        Jump();
        _IsWallRidingBool.Value = false;
        //set the value of _DetectedWallObject to the value of the hit obj from ray
        if (_ShortestRayPrefab.Value.RayHit())
        { 
            _DetectedWallObject.Value = _ShortestRayPrefab.Value.RayCastHit.collider.gameObject;
        }
    }

    private void OnWallRideOnStarted(InputAction.CallbackContext context)
    {
        _IsWallRidePressed = context.ReadValue<float>() >= 0.45f;
    }

    private void OnWallRideCanceled(InputAction.CallbackContext context)
    {
        _IsWallRidePressed = false;
        if (_ShortestRayPrefab.Value.RayHit())
        { 
            _DetectedWallObject.Value = _ShortestRayPrefab.Value.RayCastHit.collider.gameObject;
        }
        
    }
    
    private void OnWallRideLookOnperformed(InputAction.CallbackContext context)
    {
        _CameraInputVector.Value = context.ReadValue<Vector2>();
    }
    private void OnWallRideLookOncanceled(InputAction.CallbackContext context)
    {
      _CameraInputVector.Value = Vector2.zero; 
    }
    
    public void Jump()
    {
        _Rigidbody.velocity = Vector3.zero;
        _PlayerJumpCounter.Value--;
        _JumpVector = ((-_HorizontalDirectionToNormal.Value.normalized+transform.up) * _JumpMagnitude);
        _Rigidbody.AddForce(_JumpVector, ForceMode.Impulse);
    } 

    public void WallRide()
    {
        if (_CanWallRideBool.Value == true)//if can wall ride is true
        {
            if (_IsWallRidingBool.Value == true) //if is wallriding is true then add force along vector to ride and on horizontal 
            {
                SetPlayerAlignmentToWall(_VectorAlongRunnableWall.Value);
                _Rigidbody.AddForce(_VectorAlongRunnableWall.Value * _WallRideSpeedMagnitude);
                _Rigidbody.AddForce(_HorizontalDirectionToNormal.Value * _PushToWallForceMagnitude);
                _GravityController.enabled = false;
            }
        }
        else
        {
            _IsWallRidingBool.Value = false;
            _GravityController.enabled = true;
        }
    }
    
    private void ValidateWallRideBool()
    {
        if (_CanWallRideBool.Value == true)
        {
            if (_IsWallRidePressed == true)
            {
                _IsWallRidingBool.Value = true;
            }
            else
            {
                _IsWallRidingBool.Value = false;
            }
        }
        else
        {
            _IsWallRidingBool.Value = false;
        }
    }

    private void SetPlayerAlignmentToWall(Vector3 targetVector)
    {
        float snapSpeed = _SnapSpeed * Time.deltaTime;
        Vector3 faceWallVector = Vector3.RotateTowards(transform.forward, targetVector, snapSpeed, 0.0f);

        transform.localRotation = Quaternion.LookRotation(faceWallVector);
    }

    protected override void SubscribeInputActions()
    {
        _InputActionAsset.WallRideInputMap.OnWallRide.started += OnWallRideOnStarted;
        _InputActionAsset.WallRideInputMap.OnWallRide.canceled += OnWallRideCanceled;
        _InputActionAsset.WallRideInputMap.WallRideJump.performed += OnWallRideJumpPerformed;
        _InputActionAsset.WallRideInputMap.OnWallRideLook.performed+= OnWallRideLookOnperformed;
        _InputActionAsset.WallRideInputMap.OnWallRideLook.canceled += OnWallRideLookOncanceled;
    }

  

    protected override void UnsubscribeInputActions()
    {
        _InputActionAsset.WallRideInputMap.OnWallRide.started -= OnWallRideOnStarted;
        _InputActionAsset.WallRideInputMap.OnWallRide.canceled -= OnWallRideCanceled;
        _InputActionAsset.WallRideInputMap.WallRideJump.performed -= OnWallRideJumpPerformed;
        _InputActionAsset.WallRideInputMap.OnWallRideLook.performed-= OnWallRideLookOnperformed;
        _InputActionAsset.WallRideInputMap.OnWallRideLook.canceled -= OnWallRideLookOncanceled;
    }
}
