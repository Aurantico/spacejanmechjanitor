﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePlayerState
{
    protected bool _IsInState;
    protected PlayerController _Controller;
    public abstract void Tick();
    public abstract void PhysicsTick();
    
    public virtual void OnStateEnter(){}
    
    public virtual void OnStateExit(){}
    
    
}
