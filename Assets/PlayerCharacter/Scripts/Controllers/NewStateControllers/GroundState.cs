﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundState : BasePlayerState, IMovement, ILook,IJump
{ 
    
    public override void OnStateEnter()
    {
        _IsInState = true;
    }

    public override void OnStateExit()
    {
        _IsInState = false;
    }
    
    public override void Tick()
    {
        if (_IsInState == false)
        {
            return;
        }
        //DoStuff
        //switch
    }

    public override void PhysicsTick()
    {
        if (_IsInState == false)
        {
            return;
        }
        //DoStuff
    }


    public void Move()
    {
        
    }

    public void Look(float axisValueX, float axisValueY)
    {
        
    }

    public void Jump()
    {
        
    }
}
