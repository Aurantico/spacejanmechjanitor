﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState 
{
  GROUNDED,
  AERIAL,
  WALLRIDE,
  GRIND,
  COMBAT
}
