﻿using Cinemachine;
using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class InteractWithTrack : MonoBehaviour
{
    [SerializeField]
    [Required]
    private CinemachineSmoothPath _RailTrack;

    [SerializeField]
    [Required]
    private CinemachineDollyCart _Cart;

    [SerializeField]
    private RefBool _IsOnGrindRailBool;

    private bool _OnThisRail;

    private Transform _Player;

    [SerializeField]
    [Required]
    private CinemachineVirtualCamera _VCam;

    private void Awake()
    {
        _IsOnGrindRailBool.Value = false;
    }

    public void SetGrindVariables(GameObject player)
    {
        player.GetComponent<GrindController>().SetVariables(_RailTrack, _Cart, this);
    }

    public void GetOn(GameObject player)
    {
        if (_OnThisRail) return;

        _IsOnGrindRailBool.Value = true;

        _Player = player.transform;
        _Player.SetParent(_Cart.transform);
        _Player.localPosition = Vector3.zero;
        _Player.localRotation = Quaternion.identity;
        _Player.GetComponent<Rigidbody>().isKinematic = true;
        _VCam.gameObject.SetActive(false);
        _Cart.gameObject.SetActive(true);
        _OnThisRail = true;
    }

    public void GetOff(GameObject player)
    {
        _IsOnGrindRailBool.Value = false;
        player.transform.SetParent(null);
        _Player.GetComponent<Rigidbody>().isKinematic = false;
        _Player.localRotation = Quaternion.identity;
        ResetCartLocalPos();
        _VCam.gameObject.SetActive(false);
        _Cart.gameObject.SetActive(false);
        _OnThisRail = false;
    }

    private void ResetCartLocalPos()
    {
        _Cart.transform.localPosition = Vector3.zero;
        _Cart.transform.localRotation = Quaternion.identity;
        _Cart.m_Position = 0.0f;
    }
}
