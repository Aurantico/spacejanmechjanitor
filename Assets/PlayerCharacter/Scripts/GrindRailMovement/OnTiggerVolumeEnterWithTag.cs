﻿using GameSavvy.ScriptableLibrary;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class OnTiggerVolumeEnterWithTag : MonoBehaviour
{
    [SerializeField] private string _Tag = "Player";
    [SerializeField] private UEvent_GameObject _OnTriggerEnter;
    [SerializeField] private UEvent_GameObject _OnTriggerExit;


    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_Tag))
        {
            _OnTriggerEnter.Invoke(other.gameObject);
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_Tag))
        {
            _OnTriggerExit.Invoke(other.gameObject);
        }
    }
}
