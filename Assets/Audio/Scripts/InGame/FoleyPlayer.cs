﻿using UnityEngine;

public class FoleyPlayer : MonoBehaviour
{
    // [Header("Events")] 
    // [SerializeField]
    // private AK.Wwise.Event _Footsteps = null;


    // [Header("Switches")]
    // [SerializeField]
    // private AK.Wwise.Switch _Concrete = null;
    // [SerializeField]
    // private AK.Wwise.Switch _Gravel = null;


    // [Header("Speed")]
    // [SerializeField]
    // private AK.Wwise.Switch _Idle = null;
    // [SerializeField]
    // private AK.Wwise.Switch _Jogging = null;
    // [SerializeField]
    // private AK.Wwise.Switch _Sprinting = null;
    // [SerializeField]
    // private AK.Wwise.Switch Nitro = null;


    // Play footstep sound
    private void OnFootstep()
    {
        // // Null check prevents NullReferenceException
        // if (_Footsteps != null)
        // {
        //     _Footsteps.Post(this.gameObject); // same as this AkSoundEngine.PostEvent(eventName, this.gameObject)
        // }
    }

    // Set speed on animation
    private void SetSpeed(string speed)
    {
        // if (_Idle != null && speed == "Idle")
        // {
        //     _Idle.SetValue(this.gameObject);
        // }

        // else if (_Jogging != null && speed == "Jogging")
        // {
        //     _Jogging.SetValue(this.gameObject);
        // }

        // else if (_Sprinting != null && speed == "Sprinting")
        // {
        //     _Sprinting.SetValue(this.gameObject);
        // }

        // else if (Nitro != null && speed == "Nitro")
        // {
        //     Nitro.SetValue(this.gameObject);
        // }
    }

    // Set surface on trigger enter
    private void OnTriggerEnter(Collider other)
    {
        // if (_Concrete != null && other.gameObject.tag == "Concrete")
        // {
        //     _Concrete.SetValue(this.gameObject);
        // }

        // else if (_Gravel != null && other.gameObject.tag == "Gravel")
        // {
        //     _Gravel.SetValue(this.gameObject);
        // }
    }
}
