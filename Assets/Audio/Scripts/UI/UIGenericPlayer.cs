﻿using UnityEngine;

public class UIGenericPlayer : MonoBehaviour
{
    // [Header("Generic Menu")]
    // [SerializeField]
    // private AK.Wwise.Event _OnSelect = null;
    // [SerializeField]
    // private AK.Wwise.Event _OnSubmit = null;

    virtual public void PlaySelect() {}
    // => _OnSelect.Post(this.gameObject);
    virtual public void PlaySubmit() {}
    // => _OnSubmit.Post(this.gameObject);

    // Matt's tip: you can use [ContextMenu("place_a_name_here")] to allow functions to be called from the Component menu.
}
