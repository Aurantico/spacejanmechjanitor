﻿using UnityEngine;

public class UIMainMenuPlayer : UIGenericPlayer
{
    // [Header("Main Menu")]
    // [SerializeField]
    // private AK.Wwise.Event _OnCancel = null;
    // [SerializeField]
    // private AK.Wwise.Event _ExitButton = null;
    // [SerializeField]
    // private AK.Wwise.Event _StartGame = null;

    public void PlayCancel() {}
    // => _OnCancel.Post(this.gameObject);
    public void PlayExit() {}
    // => _ExitButton.Post(this.gameObject);
    public void PlayStart() {}
    // => _StartGame.Post(this.gameObject);
}
