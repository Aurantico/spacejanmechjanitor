using GameSavvy.OpenUnityAttributes;
using System.Collections;
using UnityEngine;

namespace ObjectPooling
{
    public class PoolableObject : MonoBehaviour
    {
        [SerializeField]
        private bool _AutoDisable = true;
        [SerializeField, EnableIf("_AutoDisable"), Tooltip("If Lifespan < 0, then it will not auto disable.")]
        private float _Lifespan = 3f;
        public int PoolIdIdentifier;

        #region Unity Function
        private void OnEnable()
        {
            if (_AutoDisable == false) return;

            if (_Lifespan >= 0f)
            {
                StartCoroutine(Disabler());
            }
        }

        private void OnDisable()
        {
            // Add object back to queue.
            PoolManager.AddObjectToQueue(this, PoolIdIdentifier);

            StopAllCoroutines();
        }

        private IEnumerator Disabler()
        {
            yield return new WaitForSeconds(_Lifespan);
            gameObject.SetActive(false);
        }

        #endregion
    }
}