﻿using UnityEngine;

public class UIExitConfirmation : UIScreen
{
    public void Confirm()
    {
        Application.Quit();
    }

    public void Reject()
    {
        _UIManager.Value.ShowScreen<UIPause>();
    }

    protected override void CustomOnCancelPerformed()
    {
        Reject();
    }
}
