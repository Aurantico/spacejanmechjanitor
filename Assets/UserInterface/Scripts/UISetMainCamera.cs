﻿using UnityEngine;

public class UISetMainCamera : MonoBehaviour
{
    [SerializeField]
    private RefCamera _MainCamera;

    private void Awake()
    {
        _MainCamera.Value = GetComponent<Camera>();
    }
}
