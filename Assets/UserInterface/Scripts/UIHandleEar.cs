﻿using UnityEngine;
using UnityEngine.UI;

public class UIHandleEar : MonoBehaviour
{
    private RectTransform _RectTransform;
    private Slider _HandleSlider;

    [SerializeField]
    private float _MaxValue = 20f;
    [SerializeField]
    private float _MinValue = -80f;

    private void Awake()
    {
        GetComponents();
        SubscribeEvents();
    }

    private void OnEnable()
    {
        SetEarPosition(_HandleSlider.value);
    }

    private void GetComponents()
    {
        _RectTransform = GetComponent<RectTransform>();
        _HandleSlider = GetComponentInParent<Slider>();
    }

    private void SubscribeEvents()
    {
        _HandleSlider.onValueChanged.AddListener(SetEarPosition);
    }

    private void SetEarPosition(float sliderValue)
    {
        float angle = Mathf.Lerp(_MinValue, _MaxValue, sliderValue);

        _RectTransform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, angle));
    }
}
