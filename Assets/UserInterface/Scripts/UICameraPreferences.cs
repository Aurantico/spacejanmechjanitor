﻿using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UICameraPreferences : MonoBehaviour
{
    [Header("Referenced Scriptable Objects")]
    [SerializeField]
    private RefBool _InvertedCamera;
    [SerializeField]
    private RefFloat _CameraSensitivityX;
    [SerializeField]
    private RefFloat _CameraSensitivityY;

    [Header("UI Elements")]
    [SerializeField]
    private Toggle _InvertedYToggle;
    [SerializeField]
    private Slider _CameraSenXSlider;
    [SerializeField]
    private Slider _CameraSenYSlider;
    [SerializeField]
    private TextMeshProUGUI _CameraXText;
    [SerializeField]
    private TextMeshProUGUI _CameraYText;

    [Header("Sensitivity Values")]
    [SerializeField]
    private float _MinCamXValue = 1;
    [SerializeField]
    private float _MaxCamXValue = 700;
    [SerializeField]
    private float _MinCamYValue = 1;
    [SerializeField]
    private float _MaxCamYValue = 10;

    #region Unity Methods
    private void OnEnable()
    {
        LoadSettings();
        SubscribeSetters();
    }
    #endregion

    #region Slider Methods
    private void SetInvertedYValue()
    {
        _InvertedCamera.Value = !_InvertedYToggle.isOn;
    }

    private void SetCameraSenXValue()
    {
        _CameraSensitivityX.Value = CamSenXValue();
    }

    private void SetCameraSenYValue()
    {
        _CameraSensitivityY.Value = CamSenYValue();
    }

    private void SubscribeSetters()
    {
        _CameraSenXSlider.onValueChanged.AddListener(delegate { SetCameraSenXValue(); });
        _CameraSenYSlider.onValueChanged.AddListener(delegate { SetCameraSenYValue(); });
        _InvertedYToggle.onValueChanged.AddListener(delegate { SetInvertedYValue(); });
    }

    private void LoadSettings()
    {
        _CameraSenXSlider.value = (float)System.Math.Round(Mathf.InverseLerp(_MinCamXValue, _MaxCamXValue, SaveLoadManager.Instance.SavedData.CurrentGameData.CameraSensitivityX), 1);
        _CameraSenYSlider.value = (float)System.Math.Round(Mathf.InverseLerp(_MinCamYValue, _MaxCamYValue, SaveLoadManager.Instance.SavedData.CurrentGameData.CameraSensitivityY), 1);
        _InvertedYToggle.isOn = !SaveLoadManager.Instance.SavedData.CurrentGameData.InvertedY;
    }

    public void ApplyChanges()
    {
        SaveLoadManager.Instance.SavedData.CurrentGameData.CameraSensitivityX = CamSenXValue();
        SaveLoadManager.Instance.SavedData.CurrentGameData.CameraSensitivityY = CamSenYValue();
        SaveLoadManager.Instance.SavedData.CurrentGameData.InvertedY = !_InvertedYToggle.isOn;
        SaveLoadManager.Instance.Save();
    }

    public void RevertChanges()
    {
        _CameraSenXSlider.value = (float)System.Math.Round(Mathf.InverseLerp(_MinCamXValue, _MaxCamXValue, 300), 1);
        _CameraSenYSlider.value = (float)System.Math.Round(Mathf.InverseLerp(_MinCamYValue, _MaxCamYValue, 2), 1);
        _InvertedYToggle.isOn = false;
    }

    private float CamSenXValue()
    {
        float camXsensitivity = Mathf.Round(Mathf.Lerp(_MinCamXValue, _MaxCamXValue, _CameraSenXSlider.value));
        return camXsensitivity;
    }

    private float CamSenYValue()
    {
        float camYsensitivity = Mathf.Round(Mathf.Lerp(_MinCamYValue, _MaxCamYValue, _CameraSenYSlider.value));
        return camYsensitivity;
    }

    public void DisplayNumbers()
    {
        _CameraXText.text = Mathf.Round(_CameraSenXSlider.value * 100).ToString();
        _CameraYText.text = Mathf.Round(_CameraSenYSlider.value * 100).ToString();
    }
    #endregion
}
