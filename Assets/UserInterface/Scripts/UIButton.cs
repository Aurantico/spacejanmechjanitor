﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using GameSavvy.OpenUnityAttributes;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Animator))]
public class UIButton : MonoBehaviour, ISelectHandler, ISubmitHandler, IDeselectHandler
{
    [SerializeField]
    private bool _SwapSpritesOnEvents;
    [Header("Sprite Images")]
    [SerializeField, ShowIf("_SwapSpritesOnEvents")]
    private Sprite _NormalSprite;
    [SerializeField, ShowIf("_SwapSpritesOnEvents")]
    private Sprite _SelectedSprite;
    [SerializeField, ShowIf("_SwapSpritesOnEvents")]
    private Sprite _PressedSprite;

    private Button _Button;
    private IEnumerator _CurrentCoroutine;

    public UnityEvent OnPressDown;

    #region Unity Methods
    private void Awake()
    {
        GetComponents();
    }

    private void OnEnable()
    {
        // Reset sprite to normal if the sprite is supposed to change.
        if (_SwapSpritesOnEvents == true) SetFirstSprite();
    }
    #endregion

    #region Initializing Methods
    private void GetComponents()
    {
        _Button = GetComponent<Button>();
    }

    private void SetFirstSprite()
    {
        ChangeButtonSprite(_NormalSprite);
    }
    #endregion

    #region Event Methods

    public void OnSelect(BaseEventData eventData)
    {
        DoSelect();
    }

    private void DoSelect()
    {
        if (_SwapSpritesOnEvents == true) ChangeButtonSprite(_SelectedSprite);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        if (_SwapSpritesOnEvents == true) ChangeButtonSprite(_PressedSprite);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if (_SwapSpritesOnEvents == true) ChangeButtonSprite(_NormalSprite);
    }
    #endregion

    #region Private Methods
    private void ChangeButtonSprite(Sprite sprite)
    {
        _Button.image.sprite = sprite;
    }

    private void InvokeOnPressDown()
    {
        OnPressDown?.Invoke();
    }
    #endregion
}
