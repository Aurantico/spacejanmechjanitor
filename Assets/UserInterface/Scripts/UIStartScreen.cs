﻿public class UIStartScreen : UIScreen
{
    protected override void CustomOnStartPerformed()
    {
        _UIManager.Value.ShowScreen<UIMainMenu>();
    }

    protected override void CustomOnSubmitPerformed()
    {
        _UIManager.Value.ShowScreen<UIMainMenu>();
    }
}
