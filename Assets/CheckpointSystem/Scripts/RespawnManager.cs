﻿using System;
using UnityEngine;

public class RespawnManager : MonoBehaviour
{
    [SerializeField]
    private RefRespawnManager _RespawnManager;
    [SerializeField]
    private RefTransform _PlayerTransform;
    [SerializeField]
    private RefUserInterfaceManager _UIManager;

    private Transform _CurrentCheckpoint;
    public int CurrentCheckpointIndex { get; set; }

    public event Action OnDeath;

    private void Awake()
    {
        _RespawnManager.Value = this;
        if (_UIManager.Value == null)
        {
            Debug.LogWarning($"The UI Manager is not in the scene!");
            return;
        }

        CurrentCheckpointIndex = SaveLoadManager.Instance.SavedData.CurrentGameData.CurrentCheckpointIndex;
    }

    private void Start()
    {
        if (CurrentCheckpointIndex > 0)
        {
            FindAndMatchCurrentCheckpoint();
            SetPlayerPosition();
        }
    }

    public void Respawn()
    {
        OnDeath?.Invoke();

        if (_UIManager.Value != null)
        {
            _UIManager.Value.FadeIntoScene<UIInGame>(3, true, 0.5f);
        }
        else
        {
            _PlayerTransform.Value.position = _CurrentCheckpoint.position;
        }
    }

    public void SetPlayerPosition()
    {
        _PlayerTransform.Value.position = _CurrentCheckpoint.position;
    }

    public void SetNewCheckpoint(Transform checkpoint)
    {
        _CurrentCheckpoint = checkpoint;

        if (SaveLoadManager.Instance != null)
        {
            SaveLoadManager.Instance.SavedData.CurrentGameData.CurrentCheckpointIndex = CurrentCheckpointIndex;
            SaveLoadManager.Instance.Save();
        }
    }

    private void FindAndMatchCurrentCheckpoint()
    {
        foreach (var checkpoint in Checkpoint.CheckpointList)
        {
            if(checkpoint.CurrentCheckpointIndex == CurrentCheckpointIndex)
            {
                SetNewCheckpoint(checkpoint.transform.parent);
            }
        }
    }

}
