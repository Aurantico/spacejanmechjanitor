// GENERATED AUTOMATICALLY FROM 'Assets/SpaceJanInputAssets/SpaceJanInputActionAsset.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @SpaceJanInputActionAsset : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @SpaceJanInputActionAsset()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""SpaceJanInputActionAsset"",
    ""maps"": [
        {
            ""name"": ""GroundInputMap"",
            ""id"": ""7c0b5572-6826-416a-898e-2569c6a9f624"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""9ab2aa82-28c0-4cbc-9bbc-650f3623d75c"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""StickDeadzone(min=0.25)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""8e7c2847-31df-41bd-a9a3-7a2cbe316a02"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""StickDeadzone(min=0.19)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""bd31676b-5552-4c38-9ca1-fd00fb5ef6fa"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""WallRideGround"",
                    ""type"": ""Value"",
                    ""id"": ""2da7ec0f-268f-46e1-bc76-1d6c340c335f"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b60f57bf-ac8b-4729-8ebc-0409f5cf2336"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7ad5afda-88d9-4458-91ec-9bd1bfb9ae5d"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c363896-6bcd-4156-89b1-b4209347aba0"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fb3e3404-9726-4575-9539-77b528df52d3"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WallRideGround"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""AerialInputMap"",
            ""id"": ""0c09b90e-7d97-4f99-b925-056338156b04"",
            ""actions"": [
                {
                    ""name"": ""AerialMove"",
                    ""type"": ""Value"",
                    ""id"": ""6e7b5c1e-8977-4758-a239-aaebb951d96a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""StickDeadzone(min=0.25)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AerialLook"",
                    ""type"": ""Value"",
                    ""id"": ""3693646f-2e43-42bb-8aa2-dbbf7ef037d6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""AerialJump"",
                    ""type"": ""Button"",
                    ""id"": ""e6d21138-45e9-4fb0-a6a2-beaa3dbc2c3f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""942a91ed-6910-41e0-b716-5ac07c242697"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""WallRide"",
                    ""type"": ""Value"",
                    ""id"": ""3c4436bc-b07b-40bb-9da2-08afbd53aceb"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2a5b6d4d-763a-478a-8c1e-55396422b229"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AerialMove"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1202244b-69c0-4fcf-95e4-adcc2bf75450"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AerialLook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b2622665-6451-47ed-b7ec-968f1cc7b80c"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AerialJump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""92cb107f-c34f-47a7-994d-5539aceae328"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""48f187df-8546-45e7-bc84-d2b928c85aa9"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WallRide"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""WallRideInputMap"",
            ""id"": ""73c47f47-208a-407f-9722-5e69de937502"",
            ""actions"": [
                {
                    ""name"": ""OnWallRide"",
                    ""type"": ""Value"",
                    ""id"": ""b63bd1f4-45f5-4cd9-ae2c-9a7017cc592c"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""WallRideJump"",
                    ""type"": ""Button"",
                    ""id"": ""8de28168-329b-4ff0-bf32-bb72a16f2d5d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""OnWallRideMove"",
                    ""type"": ""Button"",
                    ""id"": ""7ddec7f4-fc8d-447b-a11e-74c713be27f2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""OnWallRideLook"",
                    ""type"": ""Value"",
                    ""id"": ""a83dff78-2fdb-4afd-8e70-9f74ce94f93a"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0733e70f-b9da-4f22-ab8f-6272629b625d"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OnWallRide"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10ad6b3c-5b95-4a3c-a0dd-8d03fecafc3d"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WallRideJump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ec254864-63f5-4ee5-ada3-799b03c6400e"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OnWallRideMove"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a049639d-4c96-4a77-a9b6-d912139c0f60"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OnWallRideLook"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""GrindInputMap"",
            ""id"": ""7bebf1a9-8a3a-4256-8ea1-70fc144cfe79"",
            ""actions"": [
                {
                    ""name"": ""New action"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ecb8c6a1-4664-4eb1-bb4a-942dc232034f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""16cd1951-d1f6-46c0-bc47-04ed472885ff"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""New action"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""CombatInputMap"",
            ""id"": ""6a40f480-6e1b-463d-8848-916a3808e390"",
            ""actions"": [
                {
                    ""name"": ""North"",
                    ""type"": ""Button"",
                    ""id"": ""4932df2d-a0d1-409c-a368-4d844357f50e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""South"",
                    ""type"": ""Button"",
                    ""id"": ""4d44c010-d133-4f16-b473-da63ac91dc42"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""East"",
                    ""type"": ""Button"",
                    ""id"": ""da8d412d-1406-491f-be6b-8a63fc2f8ea1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""West"",
                    ""type"": ""Button"",
                    ""id"": ""9ea09d93-b3d0-47f9-9ed7-031dec7e4082"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""8fcc7b07-c06e-422f-830e-b3d49b8d38af"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""North"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""953b69bf-db29-4c0d-9ba0-37a99dba843f"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""South"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e2d65211-bc0c-498d-b05d-619b7cfa9063"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""East"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5aea313d-f022-4648-bcba-c8539338ff3d"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""West"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UserInterfaceInputMap"",
            ""id"": ""7fd1ab27-33a9-4d4c-a7a6-cef9d81d8f8d"",
            ""actions"": [
                {
                    ""name"": ""Navigate"",
                    ""type"": ""Value"",
                    ""id"": ""6fcd8726-779f-4b6d-a332-0b71647f4c71"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Submit"",
                    ""type"": ""Button"",
                    ""id"": ""ef3ac3e9-9238-4624-a316-c1d21bbb809b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""b48fc62d-cabb-46e6-9cbb-aa3f8dca5abe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""2371ea1d-9aea-4eed-9fe3-541015c2b108"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Gamepad"",
                    ""id"": ""ad6e6d18-9118-4f19-8c82-b70c38527eb1"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d2c22ca9-ec33-4d65-bd64-5d5a21fea718"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""8e60a115-d72d-4a20-85cb-962c49e9b1bc"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""555b5ca2-32e0-4ac6-a808-1e93e1e9bbfa"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""ba26661f-ad9f-4149-be65-ebeeb7cadba0"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""85ca13e8-d825-4978-8a53-554f5510956c"",
                    ""path"": ""<Gamepad>/dpad"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Joystick"",
                    ""id"": ""bb70cc8b-e0c3-4a4d-8526-5db15720a4b4"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Navigate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d6f8b30b-32da-4c0b-bd78-dbd66424e830"",
                    ""path"": ""<Joystick>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""7c4c3949-970f-45f2-9d39-4cc71a1f4f4c"",
                    ""path"": ""<Joystick>/stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""84275563-60d4-4b6d-b794-a2b6888678bb"",
                    ""path"": ""<Joystick>/stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""829af4ce-dbd5-4066-a497-9e18dc52fdea"",
                    ""path"": ""<Joystick>/stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Navigate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5421c8fb-0772-4167-bc72-57068fef4f03"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Submit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c37ddca-9873-4179-a83b-f513743411b7"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c71d775b-ddf2-497e-8137-c347d9cb6980"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // GroundInputMap
        m_GroundInputMap = asset.FindActionMap("GroundInputMap", throwIfNotFound: true);
        m_GroundInputMap_Move = m_GroundInputMap.FindAction("Move", throwIfNotFound: true);
        m_GroundInputMap_Look = m_GroundInputMap.FindAction("Look", throwIfNotFound: true);
        m_GroundInputMap_Jump = m_GroundInputMap.FindAction("Jump", throwIfNotFound: true);
        m_GroundInputMap_WallRideGround = m_GroundInputMap.FindAction("WallRideGround", throwIfNotFound: true);
        // AerialInputMap
        m_AerialInputMap = asset.FindActionMap("AerialInputMap", throwIfNotFound: true);
        m_AerialInputMap_AerialMove = m_AerialInputMap.FindAction("AerialMove", throwIfNotFound: true);
        m_AerialInputMap_AerialLook = m_AerialInputMap.FindAction("AerialLook", throwIfNotFound: true);
        m_AerialInputMap_AerialJump = m_AerialInputMap.FindAction("AerialJump", throwIfNotFound: true);
        m_AerialInputMap_Dash = m_AerialInputMap.FindAction("Dash", throwIfNotFound: true);
        m_AerialInputMap_WallRide = m_AerialInputMap.FindAction("WallRide", throwIfNotFound: true);
        // WallRideInputMap
        m_WallRideInputMap = asset.FindActionMap("WallRideInputMap", throwIfNotFound: true);
        m_WallRideInputMap_OnWallRide = m_WallRideInputMap.FindAction("OnWallRide", throwIfNotFound: true);
        m_WallRideInputMap_WallRideJump = m_WallRideInputMap.FindAction("WallRideJump", throwIfNotFound: true);
        m_WallRideInputMap_OnWallRideMove = m_WallRideInputMap.FindAction("OnWallRideMove", throwIfNotFound: true);
        m_WallRideInputMap_OnWallRideLook = m_WallRideInputMap.FindAction("OnWallRideLook", throwIfNotFound: true);
        // GrindInputMap
        m_GrindInputMap = asset.FindActionMap("GrindInputMap", throwIfNotFound: true);
        m_GrindInputMap_Newaction = m_GrindInputMap.FindAction("New action", throwIfNotFound: true);
        // CombatInputMap
        m_CombatInputMap = asset.FindActionMap("CombatInputMap", throwIfNotFound: true);
        m_CombatInputMap_North = m_CombatInputMap.FindAction("North", throwIfNotFound: true);
        m_CombatInputMap_South = m_CombatInputMap.FindAction("South", throwIfNotFound: true);
        m_CombatInputMap_East = m_CombatInputMap.FindAction("East", throwIfNotFound: true);
        m_CombatInputMap_West = m_CombatInputMap.FindAction("West", throwIfNotFound: true);
        // UserInterfaceInputMap
        m_UserInterfaceInputMap = asset.FindActionMap("UserInterfaceInputMap", throwIfNotFound: true);
        m_UserInterfaceInputMap_Navigate = m_UserInterfaceInputMap.FindAction("Navigate", throwIfNotFound: true);
        m_UserInterfaceInputMap_Submit = m_UserInterfaceInputMap.FindAction("Submit", throwIfNotFound: true);
        m_UserInterfaceInputMap_Cancel = m_UserInterfaceInputMap.FindAction("Cancel", throwIfNotFound: true);
        m_UserInterfaceInputMap_Pause = m_UserInterfaceInputMap.FindAction("Pause", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // GroundInputMap
    private readonly InputActionMap m_GroundInputMap;
    private IGroundInputMapActions m_GroundInputMapActionsCallbackInterface;
    private readonly InputAction m_GroundInputMap_Move;
    private readonly InputAction m_GroundInputMap_Look;
    private readonly InputAction m_GroundInputMap_Jump;
    private readonly InputAction m_GroundInputMap_WallRideGround;
    public struct GroundInputMapActions
    {
        private @SpaceJanInputActionAsset m_Wrapper;
        public GroundInputMapActions(@SpaceJanInputActionAsset wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_GroundInputMap_Move;
        public InputAction @Look => m_Wrapper.m_GroundInputMap_Look;
        public InputAction @Jump => m_Wrapper.m_GroundInputMap_Jump;
        public InputAction @WallRideGround => m_Wrapper.m_GroundInputMap_WallRideGround;
        public InputActionMap Get() { return m_Wrapper.m_GroundInputMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GroundInputMapActions set) { return set.Get(); }
        public void SetCallbacks(IGroundInputMapActions instance)
        {
            if (m_Wrapper.m_GroundInputMapActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnMove;
                @Look.started -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnLook;
                @Jump.started -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnJump;
                @WallRideGround.started -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnWallRideGround;
                @WallRideGround.performed -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnWallRideGround;
                @WallRideGround.canceled -= m_Wrapper.m_GroundInputMapActionsCallbackInterface.OnWallRideGround;
            }
            m_Wrapper.m_GroundInputMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @WallRideGround.started += instance.OnWallRideGround;
                @WallRideGround.performed += instance.OnWallRideGround;
                @WallRideGround.canceled += instance.OnWallRideGround;
            }
        }
    }
    public GroundInputMapActions @GroundInputMap => new GroundInputMapActions(this);

    // AerialInputMap
    private readonly InputActionMap m_AerialInputMap;
    private IAerialInputMapActions m_AerialInputMapActionsCallbackInterface;
    private readonly InputAction m_AerialInputMap_AerialMove;
    private readonly InputAction m_AerialInputMap_AerialLook;
    private readonly InputAction m_AerialInputMap_AerialJump;
    private readonly InputAction m_AerialInputMap_Dash;
    private readonly InputAction m_AerialInputMap_WallRide;
    public struct AerialInputMapActions
    {
        private @SpaceJanInputActionAsset m_Wrapper;
        public AerialInputMapActions(@SpaceJanInputActionAsset wrapper) { m_Wrapper = wrapper; }
        public InputAction @AerialMove => m_Wrapper.m_AerialInputMap_AerialMove;
        public InputAction @AerialLook => m_Wrapper.m_AerialInputMap_AerialLook;
        public InputAction @AerialJump => m_Wrapper.m_AerialInputMap_AerialJump;
        public InputAction @Dash => m_Wrapper.m_AerialInputMap_Dash;
        public InputAction @WallRide => m_Wrapper.m_AerialInputMap_WallRide;
        public InputActionMap Get() { return m_Wrapper.m_AerialInputMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(AerialInputMapActions set) { return set.Get(); }
        public void SetCallbacks(IAerialInputMapActions instance)
        {
            if (m_Wrapper.m_AerialInputMapActionsCallbackInterface != null)
            {
                @AerialMove.started -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialMove;
                @AerialMove.performed -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialMove;
                @AerialMove.canceled -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialMove;
                @AerialLook.started -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialLook;
                @AerialLook.performed -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialLook;
                @AerialLook.canceled -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialLook;
                @AerialJump.started -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialJump;
                @AerialJump.performed -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialJump;
                @AerialJump.canceled -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnAerialJump;
                @Dash.started -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnDash;
                @WallRide.started -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnWallRide;
                @WallRide.performed -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnWallRide;
                @WallRide.canceled -= m_Wrapper.m_AerialInputMapActionsCallbackInterface.OnWallRide;
            }
            m_Wrapper.m_AerialInputMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @AerialMove.started += instance.OnAerialMove;
                @AerialMove.performed += instance.OnAerialMove;
                @AerialMove.canceled += instance.OnAerialMove;
                @AerialLook.started += instance.OnAerialLook;
                @AerialLook.performed += instance.OnAerialLook;
                @AerialLook.canceled += instance.OnAerialLook;
                @AerialJump.started += instance.OnAerialJump;
                @AerialJump.performed += instance.OnAerialJump;
                @AerialJump.canceled += instance.OnAerialJump;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @WallRide.started += instance.OnWallRide;
                @WallRide.performed += instance.OnWallRide;
                @WallRide.canceled += instance.OnWallRide;
            }
        }
    }
    public AerialInputMapActions @AerialInputMap => new AerialInputMapActions(this);

    // WallRideInputMap
    private readonly InputActionMap m_WallRideInputMap;
    private IWallRideInputMapActions m_WallRideInputMapActionsCallbackInterface;
    private readonly InputAction m_WallRideInputMap_OnWallRide;
    private readonly InputAction m_WallRideInputMap_WallRideJump;
    private readonly InputAction m_WallRideInputMap_OnWallRideMove;
    private readonly InputAction m_WallRideInputMap_OnWallRideLook;
    public struct WallRideInputMapActions
    {
        private @SpaceJanInputActionAsset m_Wrapper;
        public WallRideInputMapActions(@SpaceJanInputActionAsset wrapper) { m_Wrapper = wrapper; }
        public InputAction @OnWallRide => m_Wrapper.m_WallRideInputMap_OnWallRide;
        public InputAction @WallRideJump => m_Wrapper.m_WallRideInputMap_WallRideJump;
        public InputAction @OnWallRideMove => m_Wrapper.m_WallRideInputMap_OnWallRideMove;
        public InputAction @OnWallRideLook => m_Wrapper.m_WallRideInputMap_OnWallRideLook;
        public InputActionMap Get() { return m_Wrapper.m_WallRideInputMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(WallRideInputMapActions set) { return set.Get(); }
        public void SetCallbacks(IWallRideInputMapActions instance)
        {
            if (m_Wrapper.m_WallRideInputMapActionsCallbackInterface != null)
            {
                @OnWallRide.started -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRide;
                @OnWallRide.performed -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRide;
                @OnWallRide.canceled -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRide;
                @WallRideJump.started -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnWallRideJump;
                @WallRideJump.performed -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnWallRideJump;
                @WallRideJump.canceled -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnWallRideJump;
                @OnWallRideMove.started -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRideMove;
                @OnWallRideMove.performed -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRideMove;
                @OnWallRideMove.canceled -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRideMove;
                @OnWallRideLook.started -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRideLook;
                @OnWallRideLook.performed -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRideLook;
                @OnWallRideLook.canceled -= m_Wrapper.m_WallRideInputMapActionsCallbackInterface.OnOnWallRideLook;
            }
            m_Wrapper.m_WallRideInputMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @OnWallRide.started += instance.OnOnWallRide;
                @OnWallRide.performed += instance.OnOnWallRide;
                @OnWallRide.canceled += instance.OnOnWallRide;
                @WallRideJump.started += instance.OnWallRideJump;
                @WallRideJump.performed += instance.OnWallRideJump;
                @WallRideJump.canceled += instance.OnWallRideJump;
                @OnWallRideMove.started += instance.OnOnWallRideMove;
                @OnWallRideMove.performed += instance.OnOnWallRideMove;
                @OnWallRideMove.canceled += instance.OnOnWallRideMove;
                @OnWallRideLook.started += instance.OnOnWallRideLook;
                @OnWallRideLook.performed += instance.OnOnWallRideLook;
                @OnWallRideLook.canceled += instance.OnOnWallRideLook;
            }
        }
    }
    public WallRideInputMapActions @WallRideInputMap => new WallRideInputMapActions(this);

    // GrindInputMap
    private readonly InputActionMap m_GrindInputMap;
    private IGrindInputMapActions m_GrindInputMapActionsCallbackInterface;
    private readonly InputAction m_GrindInputMap_Newaction;
    public struct GrindInputMapActions
    {
        private @SpaceJanInputActionAsset m_Wrapper;
        public GrindInputMapActions(@SpaceJanInputActionAsset wrapper) { m_Wrapper = wrapper; }
        public InputAction @Newaction => m_Wrapper.m_GrindInputMap_Newaction;
        public InputActionMap Get() { return m_Wrapper.m_GrindInputMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GrindInputMapActions set) { return set.Get(); }
        public void SetCallbacks(IGrindInputMapActions instance)
        {
            if (m_Wrapper.m_GrindInputMapActionsCallbackInterface != null)
            {
                @Newaction.started -= m_Wrapper.m_GrindInputMapActionsCallbackInterface.OnNewaction;
                @Newaction.performed -= m_Wrapper.m_GrindInputMapActionsCallbackInterface.OnNewaction;
                @Newaction.canceled -= m_Wrapper.m_GrindInputMapActionsCallbackInterface.OnNewaction;
            }
            m_Wrapper.m_GrindInputMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Newaction.started += instance.OnNewaction;
                @Newaction.performed += instance.OnNewaction;
                @Newaction.canceled += instance.OnNewaction;
            }
        }
    }
    public GrindInputMapActions @GrindInputMap => new GrindInputMapActions(this);

    // CombatInputMap
    private readonly InputActionMap m_CombatInputMap;
    private ICombatInputMapActions m_CombatInputMapActionsCallbackInterface;
    private readonly InputAction m_CombatInputMap_North;
    private readonly InputAction m_CombatInputMap_South;
    private readonly InputAction m_CombatInputMap_East;
    private readonly InputAction m_CombatInputMap_West;
    public struct CombatInputMapActions
    {
        private @SpaceJanInputActionAsset m_Wrapper;
        public CombatInputMapActions(@SpaceJanInputActionAsset wrapper) { m_Wrapper = wrapper; }
        public InputAction @North => m_Wrapper.m_CombatInputMap_North;
        public InputAction @South => m_Wrapper.m_CombatInputMap_South;
        public InputAction @East => m_Wrapper.m_CombatInputMap_East;
        public InputAction @West => m_Wrapper.m_CombatInputMap_West;
        public InputActionMap Get() { return m_Wrapper.m_CombatInputMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CombatInputMapActions set) { return set.Get(); }
        public void SetCallbacks(ICombatInputMapActions instance)
        {
            if (m_Wrapper.m_CombatInputMapActionsCallbackInterface != null)
            {
                @North.started -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnNorth;
                @North.performed -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnNorth;
                @North.canceled -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnNorth;
                @South.started -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnSouth;
                @South.performed -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnSouth;
                @South.canceled -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnSouth;
                @East.started -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnEast;
                @East.performed -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnEast;
                @East.canceled -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnEast;
                @West.started -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnWest;
                @West.performed -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnWest;
                @West.canceled -= m_Wrapper.m_CombatInputMapActionsCallbackInterface.OnWest;
            }
            m_Wrapper.m_CombatInputMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @North.started += instance.OnNorth;
                @North.performed += instance.OnNorth;
                @North.canceled += instance.OnNorth;
                @South.started += instance.OnSouth;
                @South.performed += instance.OnSouth;
                @South.canceled += instance.OnSouth;
                @East.started += instance.OnEast;
                @East.performed += instance.OnEast;
                @East.canceled += instance.OnEast;
                @West.started += instance.OnWest;
                @West.performed += instance.OnWest;
                @West.canceled += instance.OnWest;
            }
        }
    }
    public CombatInputMapActions @CombatInputMap => new CombatInputMapActions(this);

    // UserInterfaceInputMap
    private readonly InputActionMap m_UserInterfaceInputMap;
    private IUserInterfaceInputMapActions m_UserInterfaceInputMapActionsCallbackInterface;
    private readonly InputAction m_UserInterfaceInputMap_Navigate;
    private readonly InputAction m_UserInterfaceInputMap_Submit;
    private readonly InputAction m_UserInterfaceInputMap_Cancel;
    private readonly InputAction m_UserInterfaceInputMap_Pause;
    public struct UserInterfaceInputMapActions
    {
        private @SpaceJanInputActionAsset m_Wrapper;
        public UserInterfaceInputMapActions(@SpaceJanInputActionAsset wrapper) { m_Wrapper = wrapper; }
        public InputAction @Navigate => m_Wrapper.m_UserInterfaceInputMap_Navigate;
        public InputAction @Submit => m_Wrapper.m_UserInterfaceInputMap_Submit;
        public InputAction @Cancel => m_Wrapper.m_UserInterfaceInputMap_Cancel;
        public InputAction @Pause => m_Wrapper.m_UserInterfaceInputMap_Pause;
        public InputActionMap Get() { return m_Wrapper.m_UserInterfaceInputMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UserInterfaceInputMapActions set) { return set.Get(); }
        public void SetCallbacks(IUserInterfaceInputMapActions instance)
        {
            if (m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface != null)
            {
                @Navigate.started -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnNavigate;
                @Navigate.performed -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnNavigate;
                @Navigate.canceled -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnNavigate;
                @Submit.started -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnSubmit;
                @Submit.performed -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnSubmit;
                @Submit.canceled -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnSubmit;
                @Cancel.started -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnCancel;
                @Pause.started -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface.OnPause;
            }
            m_Wrapper.m_UserInterfaceInputMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Navigate.started += instance.OnNavigate;
                @Navigate.performed += instance.OnNavigate;
                @Navigate.canceled += instance.OnNavigate;
                @Submit.started += instance.OnSubmit;
                @Submit.performed += instance.OnSubmit;
                @Submit.canceled += instance.OnSubmit;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
            }
        }
    }
    public UserInterfaceInputMapActions @UserInterfaceInputMap => new UserInterfaceInputMapActions(this);
    public interface IGroundInputMapActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnWallRideGround(InputAction.CallbackContext context);
    }
    public interface IAerialInputMapActions
    {
        void OnAerialMove(InputAction.CallbackContext context);
        void OnAerialLook(InputAction.CallbackContext context);
        void OnAerialJump(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnWallRide(InputAction.CallbackContext context);
    }
    public interface IWallRideInputMapActions
    {
        void OnOnWallRide(InputAction.CallbackContext context);
        void OnWallRideJump(InputAction.CallbackContext context);
        void OnOnWallRideMove(InputAction.CallbackContext context);
        void OnOnWallRideLook(InputAction.CallbackContext context);
    }
    public interface IGrindInputMapActions
    {
        void OnNewaction(InputAction.CallbackContext context);
    }
    public interface ICombatInputMapActions
    {
        void OnNorth(InputAction.CallbackContext context);
        void OnSouth(InputAction.CallbackContext context);
        void OnEast(InputAction.CallbackContext context);
        void OnWest(InputAction.CallbackContext context);
    }
    public interface IUserInterfaceInputMapActions
    {
        void OnNavigate(InputAction.CallbackContext context);
        void OnSubmit(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
    }
}
