﻿using UnityEngine;

public class SpaceJanInputLoader : MonoBehaviour
{
    [SerializeField]
    private RefInputActionAsset _SpaceJanInputAsset;

    private void Awake()
    {
        if (_SpaceJanInputAsset.Value != null) return;
        _SpaceJanInputAsset.Value = new SpaceJanInputActionAsset();
    }
}
