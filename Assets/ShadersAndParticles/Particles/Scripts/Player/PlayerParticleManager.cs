﻿using UnityEngine;

public class PlayerParticleManager : MonoBehaviour
{
    [Header("Footsteps")]
    [SerializeField]
    private ParticleSystem _LeftFootstep;
    [SerializeField]
    private ParticleSystem _RightFootstep;

    [Header("Landing")]
    [SerializeField]
    private ParticleSystem _Landing;

    [Header("Hand Sparks")]
    [SerializeField]
    private ParticleSystem _LeftMetalSpark;
    [SerializeField]
    private ParticleSystem _RightMetalSpark;

    [Header("Thruster Fire")]
    [SerializeField]
    private ParticleSystem _LeftThrusterLoop;
    [SerializeField]
    private ParticleSystem _RightThrusterLoop;
    [SerializeField]
    private ParticleSystem _LeftThrusterFire;
    [SerializeField]
    private ParticleSystem _RightThrusterFire;

    [Header("Foam")]
    [SerializeField]
    private ParticleSystem _LeftFoam;
    [SerializeField]
    private ParticleSystem _RightFoam;

    private void SpawnRightFootstepParticle()
    {
        _RightFootstep.Play();
    }

    private void SpawnLeftFootstepParticle()
    {
        _LeftFootstep.Play();
    }

    public void PlayMetalSparks(bool right)
    {
        if (right == true)
        {
            _RightMetalSpark.Play();
        }
        else
        {
            _LeftMetalSpark.Play();
        }
    }

    public void StopMetalSparks()
    {
        _LeftMetalSpark.Stop();
        _RightMetalSpark.Stop();
    }

    public void PlayFireThrustersLoop()
    {
        _LeftThrusterLoop.Play();
        _RightThrusterLoop.Play();
    }

    public void StopFireThrustersLoop()
    {
        _LeftThrusterLoop.Stop();
        _RightThrusterLoop.Stop();
    }

    public void PlayFireThrusters()
    {
        _LeftThrusterFire.Play();
        _RightThrusterFire.Play();
    }

    public void PlayLanding()
    {
        _Landing.Play();
    }

    public void PlayFoam()
    {
        _LeftFoam.Play();
        _RightFoam.Play();
    }

    public void StopFoam()
    {
        _LeftFoam.Stop();
        _RightFoam.Stop();
    }
}
