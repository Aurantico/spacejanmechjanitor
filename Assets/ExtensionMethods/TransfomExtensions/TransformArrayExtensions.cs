using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class TransformArrayExtensions
{
    public static void PopulateTransformHits(this Transform[] origins, List<TransformHit> lst, float maxDistance)
    {
        if (lst == null) lst = new List<TransformHit>();
        else lst.Clear();

        foreach (var item in origins)
        {
            var trh = new TransformHit
            {
                OriginTransform = item
            };

            var ray = new Ray(item.position, item.forward);
            trh.HasHit = Physics.Raycast(ray, out var hit, maxDistance);

            trh.Hit = hit;
            lst.Add(trh);
        }

        lst = lst.OrderBy(d => d.HasHit ? d.Hit.distance : float.MaxValue).ToList();
    }
}
