﻿using System;
using UnityEngine;

public class AngleBetween : MonoBehaviour
{
    [SerializeField]
    private Transform _OtherTransform;

    private void OnGUI()
    {
        //GUILayout.Label(Vector3.Angle(transform.forward, _OtherTransform.forward).ToString());
        var cross = Vector3.Cross(transform.forward, _OtherTransform.forward);
        var dot = Vector3.Dot(transform.forward, _OtherTransform.forward);
        var angle = (1f - dot)/2f;
        angle *= Mathf.Sign(cross.y);
        
        GUILayout.Label( angle.ToString() ) ;
       
    }
}
