﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class AutoloadOnVideoEnd : MonoBehaviour
{
    [SerializeField]
    private string _SceneToLoad;

    private VideoPlayer _VideoPlayer;

    private IEnumerator _CurrentCoroutine;

    private void Start()
    {
        _VideoPlayer = GetComponent<VideoPlayer>();

        _CurrentCoroutine = VideoLength(_VideoPlayer.clip);
        StartCoroutine(_CurrentCoroutine);
    }

    private IEnumerator VideoLength(VideoClip clip)
    {
        float time = 0;

        while (time < clip.length)
        {
            time += Time.unscaledDeltaTime;
            yield return null;

            if (time >= clip.length)
            {
                SceneManager.LoadScene(_SceneToLoad);
                StopCoroutine(_CurrentCoroutine);
            }
        }
    }
}
