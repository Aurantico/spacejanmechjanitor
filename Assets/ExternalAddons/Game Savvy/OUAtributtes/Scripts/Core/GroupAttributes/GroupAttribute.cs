using System;

namespace GameSavvy.OpenUnityAttributes
{
    public abstract class GroupAttribute : OpenUnityAttribute
    {
        public string Name { get; private set; }

        public GroupAttribute(string name)
        {
            Name = name;
        }
    }
}
