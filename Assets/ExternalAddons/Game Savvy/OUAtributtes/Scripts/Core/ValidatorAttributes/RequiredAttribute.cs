using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class RequiredAttribute : ValidatorAttribute
    {
        public string Message { get; private set; }
        public bool LogToConsole { get; private set; }

        public RequiredAttribute(string message = null, bool logToConsole = false)
        {
            Message = message;
            LogToConsole = logToConsole;
        }
    }
}
