﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    // The base class for all Open Unity attributes
    public class OpenUnityAttribute : Attribute
    {
    }
}
