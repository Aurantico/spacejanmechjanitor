﻿// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class __classname__
    {
        private static Dictionary<Type, FieldDrawer> _DrawersByAttributeType;

        static __classname__()
        {
            _DrawersByAttributeType = new Dictionary<Type, FieldDrawer>();
            __entries__
        }

        public static FieldDrawer GetDrawerForAttribute(Type attributeType)
        {
            FieldDrawer drawer;
            if (_DrawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }
    }
}
