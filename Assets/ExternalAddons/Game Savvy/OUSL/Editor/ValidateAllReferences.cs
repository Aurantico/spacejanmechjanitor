﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using GameSavvy.ScriptableLibrary.Inspector;
using GameSavvy.OpenUnityAttributes;

public static class ValidateAllReferences
{
    [MenuItem("Tools/Scriptable Library/Validate All Refs #%&v")]
    public static void DoValidateAll()
    {
        int errorsFound = 0;
        int warningsFound = 0;
        int total = 0;

        MonoBehaviour[] sceneActive = GameObject.FindObjectsOfType<MonoBehaviour>();
        foreach (MonoBehaviour mono in sceneActive)
        {
            FieldInfo[] objectFields = mono.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            for (int i = 0; i < objectFields.Length; i++)
            {
                total++;

                bool onlyAssets = Attribute.IsDefined(objectFields[i], typeof(OnlyAssetsAttribute));
                bool onlySceneObjs = Attribute.IsDefined(objectFields[i], typeof(OnlySceneObjectsAttribute));
                bool required = Attribute.IsDefined(objectFields[i], typeof(RequiredAttribute));

                var val = objectFields[i].GetValue(mono);
                if (val != null && val.Equals(null) == false)
                {
                    if (typeof(IRefValidate).IsAssignableFrom(objectFields[i].FieldType))
                    {
                        var iRefVal = objectFields[i].GetValue(mono) as IRefValidate;
                        if (iRefVal == null || iRefVal.IsValid() == false)
                        {
                            errorsFound++;
                            Debug.LogError($"IRefValidate Behaviour [{GetFieldName(mono, objectFields[i]) }] : Has a pointer to a Reference with an invalid value!", mono);
                            Debug.LogError($"    => IRefValidate Asset [{GetFieldName(mono, objectFields[i]) }] : Has an invalid value!", (UnityEngine.Object)iRefVal);
                        }
                    }
                    continue;
                }

                if (required)
                {
                    errorsFound++;
                    Debug.LogError($"Required [{GetFieldName(mono, objectFields[i])}] : Requires a Reference!", mono);
                }
                else if (onlyAssets)
                {
                    warningsFound++;
                    Debug.LogWarning($"OnlyAssets [{GetFieldName(mono, objectFields[i])}] : Field is not assigned...", mono);
                }
                else if (onlySceneObjs)
                {
                    warningsFound++;
                    Debug.LogWarning($"OnlySceneObjects [{GetFieldName(mono, objectFields[i])}] : Field is not assigned...", mono);
                }
                else
                {
                    warningsFound++;
                    Debug.LogWarning($"Not Serialized [{GetFieldName(mono, objectFields[i])}] : Field is not assigned...", mono);
                }

            }
        }
        Debug.LogFormat("Finished Validating: Errors [ {0} ],   Warnings [ {1} ],   out of [ {2} ]", errorsFound, warningsFound, total);
    }

    private static string GetFieldName(MonoBehaviour mono, FieldInfo field)
    {
        return $"{mono.name} -> {mono.GetType().Name} -> {field.Name}";
    }

}

#endif
