﻿#if SCRIPTABLE_LIBRARY
#else
    #define SCRIPTABLE_LIBRARY
#endif

#if UNITY_EDITOR

namespace GameSavvy.ScriptableLibrary
{
    using System;
    using System.Linq;
    using UnityEditor;

    internal static class EnsureScriptableLibraryDefine
    {
        private const string DEFINE = "SCRIPTABLE_LIBRARY";

        [InitializeOnLoadMethod]
        private static void EnsureScriptingDefineSymbol()
        {
            var currentTarget = EditorUserBuildSettings.selectedBuildTargetGroup;

            if (currentTarget == BuildTargetGroup.Unknown)
            {
                return;
            }

            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(currentTarget).Trim();
            var defines = definesString.Split(';');

            if (defines.Contains(DEFINE) == false)
            {
                if (definesString.EndsWith(";", StringComparison.InvariantCulture) == false)
                {
                    definesString += ";";
                }

                definesString += DEFINE;

                PlayerSettings.SetScriptingDefineSymbolsForGroup(currentTarget, definesString);
            }
        }
    }
}
#else
    #define UNITY_EDITOR
#endif
