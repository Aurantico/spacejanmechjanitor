﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{

    [CustomEditor(typeof(RefQuaternion))]
    public class RefQuaternionInspector : Editor
    {
        protected RefQuaternion refVar;

        protected void OnEnable()
        {
            refVar = (RefQuaternion)target;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            Vector3 v3 = refVar.Value.eulerAngles;
            v3 = new Vector3((float)Math.Round(v3.x, 2), (float)Math.Round(v3.y, 2), (float)Math.Round(v3.z, 2));
            v3 = EditorGUILayout.Vector3Field("Value ", v3);
            refVar.Value = Quaternion.Euler(v3);

            if (GUILayout.Button("Notify Observers"))
            {
                refVar.NotifyObservers();
            }
        }
    }
}

#endif
