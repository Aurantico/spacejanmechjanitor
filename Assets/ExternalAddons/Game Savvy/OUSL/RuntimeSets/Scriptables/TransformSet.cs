﻿using UnityEngine;

namespace GameSavvy.ScriptableLibrary.RuntimeSets
{
	[System.Serializable]
	[CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/TransformSet")]
	public class TransformSet : RuntimeSet<Transform> { }
}
