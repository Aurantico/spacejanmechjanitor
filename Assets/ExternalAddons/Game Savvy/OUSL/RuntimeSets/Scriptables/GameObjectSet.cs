﻿using UnityEngine;

namespace GameSavvy.ScriptableLibrary.RuntimeSets
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/RuntimeSets/GameObjectSet")]
	public class GameObjectSet : RuntimeSet<GameObject> { }
}
