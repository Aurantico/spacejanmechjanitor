﻿using GameSavvy.ScriptableLibrary.RuntimeSets;
using UnityEngine;

public class AddToGameObjectSet : AddToTSet<GameObject, GameObjectSet>
{

	protected override void Awake()
	{
		_ToAdd = gameObject;
		if (_OnlyIfEnabed == false)
		{
			_Set.AddItem(_ToAdd);
		}
	}

}
