﻿namespace GameSavvy.ScriptableLibrary.Inspector
{
	public interface IRefValidate
	{
		bool IsValid();
	}
}
