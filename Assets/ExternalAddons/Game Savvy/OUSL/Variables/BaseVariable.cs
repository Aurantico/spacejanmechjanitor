﻿using GameSavvy.OpenUnityAttributes;
using GameSavvy.ScriptableLibrary.Inspector;
using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{

	[System.Serializable]
	public abstract class BaseVariable<T, RefVar> : IRefValidate where RefVar : RefVariable<T>
	{
		[SerializeField]
		private bool UseConstant = false;

		[SerializeField]
		private T ConstantValue;

		[SerializeField]
        [OnlyAssets]
		private RefVar ReferenceVar;

		public T Value
		{
			get
			{
				return UseConstant ? ConstantValue : ReferenceVar.Value;
			}

			set
			{
				if (UseConstant)
				{
					ConstantValue = value;
				}
				else
				{
					ReferenceVar.Value = value;
				}
			}
		}

		public bool IsValid()
		{
			if (UseConstant == false)
			{
				if(ReferenceVar == null)
				{
					return false;
				}
			}
			else if(ConstantValue == null || ConstantValue.Equals(null))
			{
				return false;
			}
			return true;
		}

	}

	[System.Serializable]
	public class BoolVariable : BaseVariable<bool, RefBool> { }

	[System.Serializable]
	public class ByteVariable : BaseVariable<byte, RefByte> { }

	[System.Serializable]
	public class UShortVariable : BaseVariable<ushort, RefUShort> { }

	[System.Serializable]
	public class UIntVariable : BaseVariable<uint, RefUInt> { }

	[System.Serializable]
	public class ULongVariable : BaseVariable<ulong, RefULong> { }

	[System.Serializable]
	public class SByteVariable : BaseVariable<sbyte, RefSByte> { }

	[System.Serializable]
	public class ShortVariable : BaseVariable<short, RefShort> { }

	[System.Serializable]
	public class IntVariable : BaseVariable<int, RefInt> { }

	[System.Serializable]
	public class LongVariable : BaseVariable<long, RefLong> { }

	[System.Serializable]
	public class FloatVariable : BaseVariable<float, RefFloat> { }

	[System.Serializable]
	public class DoubleVariable : BaseVariable<double, RefDouble> { }

	[System.Serializable]
	public class Vector2Variable : BaseVariable<Vector2, RefVector2> { }

	[System.Serializable]
	public class Vector3Variable : BaseVariable<Vector3, RefVector3> { }

	[System.Serializable]
	public class QuaternionVariable : BaseVariable<Quaternion, RefQuaternion> { }

	[System.Serializable]
	public class StringVariable : BaseVariable<string, RefString> { }

	[System.Serializable]
	public class TransformVariable : BaseVariable<Transform, RefTransform> { }

	[System.Serializable]
	public class GameObjectVariable : BaseVariable<GameObject, RefGameObject> { }

	[System.Serializable]
	public class RigidBodyVariable : BaseVariable<Rigidbody, RefRigidbody> { }

	[System.Serializable]
	public class RigidBody2DVariable : BaseVariable<Rigidbody2D, RefRigidbody2D> { }

	[System.Serializable]
	public class CameraVariable : BaseVariable<Camera, RefCamera> { }

}
