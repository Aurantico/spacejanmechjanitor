using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/UInt")]
	public class RefUInt : RefVariable<uint> { }
}
