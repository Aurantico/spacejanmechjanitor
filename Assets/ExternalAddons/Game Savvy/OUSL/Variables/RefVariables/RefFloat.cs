using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Float")]
	public class RefFloat: RefVariable<float> { }
}
