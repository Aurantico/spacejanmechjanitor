using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Bool")]
	public class RefBool : RefVariable<bool> { }
}
