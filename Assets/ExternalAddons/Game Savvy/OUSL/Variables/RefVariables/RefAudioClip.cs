using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/RefAudioClip")]
    public class RefAudioClip : RefVariable<AudioClip>
    {
        override public bool IsValid()
        {
            return _Value != null && ((_Value is null) == false);
        }
    }
}
