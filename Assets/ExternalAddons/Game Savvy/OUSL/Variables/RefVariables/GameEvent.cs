﻿using System;
using GameSavvy.OpenUnityAttributes;
using GameSavvy.ScriptableLibrary.Inspector;
using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/GameEvent")]
    public class GameEvent : ScriptableObject, IRefValidate
    {
        public event Action Observers;

        public virtual bool IsValid()
        {
            return true;
        }

        [Button]
        public void NotifyObservers()
        {
            if (Observers != null)
            {
                Observers();
            }
        }
    }
}
