using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Rigidbody2D")]
    public class RefRigidbody2D : RefVariable<Rigidbody2D> {}
}
