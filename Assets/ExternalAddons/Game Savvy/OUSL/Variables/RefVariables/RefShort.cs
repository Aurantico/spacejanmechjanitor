using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Short")]
	public class RefShort : RefVariable<short> { }
}
