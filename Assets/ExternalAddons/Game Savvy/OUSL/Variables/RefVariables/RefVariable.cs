﻿using UnityEngine;
using System;
using GameSavvy.ScriptableLibrary.Inspector;
using GameSavvy.OpenUnityAttributes;

namespace GameSavvy.ScriptableLibrary
{
    [System.Serializable]
    public abstract class RefVariable<T> : ScriptableObject, IRefValidate
    {
        public event Action<T> Observers;

        [Space]
        [SerializeField]
        [GetSet("Value")]
        protected T _Value;
        public T Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
                NotifyObservers();
            }
        }

        [Button]
        public void NotifyObservers()
        {
            if (Observers != null)
            {
                Observers(_Value);
            }
        }

        virtual public bool IsValid()
        {
            return _Value != null;
        }

    }
}
