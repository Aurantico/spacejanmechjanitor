using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Camera")]
	public class RefCamera : RefVariable<Camera> { }
}
