using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
    [CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Transform")]
    public class RefTransform : RefVariable<Transform> {}
}
