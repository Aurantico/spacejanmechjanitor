using UnityEngine;

namespace GameSavvy.ScriptableLibrary
{
	[CreateAssetMenu(menuName = "ScriptableLibrary/Variables/Vector2")]
	public class RefVector2 : RefVariable<Vector2> { }
}
