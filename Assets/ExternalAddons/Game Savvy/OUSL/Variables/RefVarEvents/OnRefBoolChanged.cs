﻿using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefBoolChanged : OnRefVariableChanged<bool, RefBool, UEvent_Bool> { }
}
