using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefUShortChanged : OnRefVariableChanged<ushort, RefUShort, UEvent_UShort> { }
}
