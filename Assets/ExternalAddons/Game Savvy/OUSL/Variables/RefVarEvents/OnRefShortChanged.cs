using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefShortChanged : OnRefVariableChanged<short, RefShort, UEvent_Short> { }
}
