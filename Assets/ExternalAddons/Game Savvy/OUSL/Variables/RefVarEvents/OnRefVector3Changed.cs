using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefVector3Changed : OnRefVariableChanged<Vector3, RefVector3, UEvent_Vector3> { }
}
