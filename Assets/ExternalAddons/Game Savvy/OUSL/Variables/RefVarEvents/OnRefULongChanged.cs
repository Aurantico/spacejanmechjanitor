using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefULongChanged : OnRefVariableChanged<ulong, RefULong, UEvent_ULong> { }
}
