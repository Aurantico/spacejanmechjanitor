﻿using GameSavvy.OpenUnityAttributes;
using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{

    public abstract class OnRefVariableChanged<T, RefVar, UEvent> : MonoBehaviour where RefVar : RefVariable<T> where UEvent : UnityEvent<T>
    {
        [Space]
        [SerializeField]
        [Required]
        [OnlyAssets]
        protected RefVar _RefVar;

        [Space]
        public UEvent Actions;

        [Space]
        [SerializeField]
        protected bool _NotifyOnEnable = false;

        private bool CanUse => (_RefVar != null && _RefVar.Value != null);

        protected void OnEnable()
        {
            if (_RefVar == null)
            {
                return;
            }

            _RefVar.Observers += TriggerActions;
            if (_NotifyOnEnable)
            {
                TriggerActionsButton();
            }
        }

        protected void OnDisable()
        {
            if (_RefVar == null)
            {
                return;
            }

            _RefVar.Observers -= TriggerActions;
        }

        private void TriggerActions(T val)
        {
            Actions.Invoke(val);
        }

        [Button]
        public void TriggerActionsButton()
        {
            if (CanUse)
            {
                TriggerActions(_RefVar.Value);
            }
        }

    }
}
