using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefStringChanged : OnRefVariableChanged<string, RefString, UEvent_String> { }
}
