using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefUIntChanged : OnRefVariableChanged<uint, RefUInt, UEvent_UInt> { }
}
