﻿using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefByteChanged : OnRefVariableChanged<byte, RefByte, UEvent_Byte> { }
}
