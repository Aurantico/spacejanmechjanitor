using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefSByteChanged : OnRefVariableChanged<sbyte, RefSByte, UEvent_SByte> { }
}
