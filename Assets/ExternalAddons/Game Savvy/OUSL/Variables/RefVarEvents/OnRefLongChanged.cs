using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefLongChanged : OnRefVariableChanged<long, RefLong, UEvent_Long> { }
}
