﻿using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.ScriptableLibrary
{
	public class OnRefGameObjectChanged : OnRefVariableChanged<GameObject, RefGameObject, UEvent_GameObject> { }
}
