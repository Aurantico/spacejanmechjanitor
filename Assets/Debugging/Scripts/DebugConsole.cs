using System;
using System.Collections.Generic;
using GameSavvy.OpenUnityAttributes;
using UnityEngine;
using UnityEngine.UI;

namespace Console   // Use 'namespace Console' on all/new ConsoleCommand related scripts
{
    // Class present on the gameObject
    public class DebugConsole : MonoBehaviour
    {
        // Singleton instance
        public static DebugConsole Instance { get; private set; }
        // Command dictionary
        public static SortedDictionary<string, ConsoleCommand> Commands { get; private set; }

        [SerializeField, ReorderableList]
        private CustomCommands[] _CustomCommands;

        // Components modified within the console
        [Header("UI Components")]
        [SerializeField]
        private GameObject _ConsoleCanvas;
        [SerializeField]
        private ScrollRect _ScrollView;
        // Input field displayed on the console
        public InputField ConsoleInput;
        // Text displayed on the window
        public Text ConsoleText;

        [HideInInspector]
        public string[] InputParts;

        // Time at which the up or down arrow key was pressed
        private float _ArrowPressTime;
        // Threshold needed before the ScrollView is called with Input.GetKey
        private float _ArrowPressThreshold = 0.5f;
        // The direction that the ScrollView is moving
        private enum Direction { Up, Down };

        public Action<ConsoleCommand> EnterCommand;

        private void Awake()
        {
            if (Instance != null)
            {
                Debug.LogError("Another instance of DebugConsole was detected.");
                Destroy(this);
                Debug.LogWarning("DebugConsole copy terminated.");
                return;
            }

            Instance = this;
            Commands = new SortedDictionary<string, ConsoleCommand>();
        }

        private void Start()
        {
            CreateCommands();
            _ConsoleCanvas.SetActive(false);
        }

        // Method used to call the custom Debug.Log system
        private void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        // mMethod used to call the custom Debug.Log system
        private void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        // Method used to call the custom Debug.Log system
        private void HandleLog(string logMessage, string stackTrace, LogType type)
        {
            // Default color for the message
            string color = "white";

            switch (type)
            {
                // White text if the message is a log
                case LogType.Log:
                    color = "white";
                    break;
                // Yellow text for warnings
                case LogType.Warning:
                    color = "yellow";
                    break;
                // Red text for errors
                case LogType.Error:
                    color = "red";
                    break;
            }

            // Change color of the text based on log type
            string _message = $"<color={color}> <b>{type.ToString()}: </b>{logMessage}</color>";
            AddMessageToConsole(_message, true);
        }

        // Creates all the commands used in the console dictionary
        private void CreateCommands()
        {
            // General commands
            CommandClear.CreateCommand();
            CommandDescribe.CreateCommand();
            CommandHelp.CreateCommand();
            CommandQuit.CreateCommand();

            foreach (var command in _CustomCommands)
            {
                command.CreateCommand();
            }


            // Tip
            AddMessageToConsole("Enter 'help' for more information.");
        }

        // Adds a command to the console
        public static void AddCommandsToConsole(string _name, ConsoleCommand _command)
        {
            // Checks if the command doesn't exist yet
            if (!Commands.ContainsKey(_name))
            {
                Commands.Add(_name, _command);
            }
        }

        private void Update()
        {
            UpdateConsoleVisibility();

            if (_ConsoleCanvas.activeInHierarchy)
            {
                UpdateOnSubmit();
                UpdateScrollRect();
            }
        }

        // Updates the console's visibility
        private void UpdateConsoleVisibility()
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                _ConsoleCanvas.SetActive(!_ConsoleCanvas.activeInHierarchy);

                if (_ConsoleCanvas.activeSelf)
                {
                    Time.timeScale = 0.0f;
                    ConsoleInput.text = "";
                    FocusInputField();
                }
                else
                {
                    Time.timeScale = 1.0f;
                }
            }
        }

        // Handles the ScrollRect's position manually with the Up and Down arrows
        private void UpdateScrollRect()
        {
            // Moves the console Up once
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _ArrowPressTime = Time.time;
                HandleScrollRectMovement(Direction.Up);
            }

            // Moves the console Up if the user holds the Up key for longer than the threshold  
            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (_ArrowPressTime + _ArrowPressThreshold <= Time.time)
                {
                    HandleScrollRectMovement(Direction.Up);
                }
            }

            // Moves the console Down once
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _ArrowPressTime = Time.time;
                HandleScrollRectMovement(Direction.Down);
            }


            // Moves the console up if the user holds the Down key for longer than the threshold  
            if (Input.GetKey(KeyCode.DownArrow))
            {
                if (_ArrowPressTime + _ArrowPressThreshold <= Time.time)
                {
                    HandleScrollRectMovement(Direction.Down);
                }
            }
        }

        // Handles the result of submitting a string on the input field
        private void UpdateOnSubmit()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                // If the input field is not empty
                if (ConsoleInput.text != "")
                {
                    AddMessageToConsole(ConsoleInput.text);
                    ParseInput(ConsoleInput.text);
                    FocusInputField();
                }
            }
        }

        // Adds a message to the console
        private void AddMessageToConsole(string input, bool log = false)
        {
            // ">" identifier for attempted commands
            string messageIdentifier = "> ";
            // If the message is a Debug.Log message the identifier disappears
            if (log) messageIdentifier = "";

            // Adds the message to the console and skips to the next line
            ConsoleText.text += messageIdentifier + input + "\n";
            // Moves the console back to the bottom
            _ScrollView.verticalNormalizedPosition = 0f;
        }

        // Attempts to parse the input
        private void ParseInput(string input)
        {
            // Splits the input and ignores null characters
            InputParts = ConsoleInput.text.Split(null, ' ');

            // Checks if the input is not empty
            if (InputParts.Length == 0 || InputParts == null)
            {
                Debug.LogWarning("Command is empty.");
                return;
            }

            // Checks if the input is recognized by the dictionary
            if (!Commands.ContainsKey(InputParts[0]))
            {
                Debug.LogWarning("Command not recognized.");
            }
            // Attempts to run the command
            else
            {
                Commands[InputParts[0]].TryRunCommand();
            }
        }

        // Selects and activates the input field
        private void FocusInputField()
        {
            ConsoleInput.Select();
            ConsoleInput.ActivateInputField();
        }

        // Moves the scroll rect based on the direction parameter received
        private void HandleScrollRectMovement(Direction direction)
        {
            // Direction multiplier
            float directionValue = 0f;
            // New scroll rect position value
            Vector2 scrollValue = new Vector2(0f, _ScrollView.scrollSensitivity);
            // Use scrollSensitivity to keep both scroll wheel and manual scroll at the same move ratio

            // Sets the multiplier based on the direction parameter
            if (direction == Direction.Up) directionValue = -1;
            else if (direction == Direction.Down) directionValue = 1;

            // Moves the scroll rect
            _ScrollView.content.anchoredPosition += scrollValue * directionValue;
        }
    }
}
