﻿using System.Collections.Generic;
using UnityEngine;

namespace Console
{
    public class CommandClear : ConsoleCommand
    {
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public CommandClear()
        {
            Command = "clear";
            Description = "Clears the developer console.";
            Help = "Use this command to clear the console.";

            AddCommandToConsole();
        }

        public override void RunCommand()
        {
            DebugConsole.Instance.ConsoleText.text = "";
        }

        public static CommandClear CreateCommand()
        {
            return new CommandClear();
        }
    }

    public class CommandDescribe : ConsoleCommand
    {
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public CommandDescribe()
        {
            Command = "describe";
            Description = "Provides a general description of the selected command.";
            Help = "Use this command (describe <i>command</i>) to get more information on other commands.";

            AddCommandToConsole();
        }

        public override void RunCommand()
        {
            foreach (KeyValuePair<string, ConsoleCommand> command in DebugConsole.Commands)
            {
                if (command.Key == DebugConsole.Instance.InputParts[1])
                {
                    Debug.Log(command.Key + " - " + command.Value.Description);
                }
            }
        }

        public override bool ValidateInput()
        {
            if (DebugConsole.Instance.InputParts.Length <= 1)
            {
                Debug.LogWarning("Missing command name. Try 'describe <b>command</b>' instead.");
                return false;
            }
            else if (DebugConsole.Instance.InputParts.Length >= 3)
            {
                Debug.LogWarning("The describe command can only take one argument.");
                return false;
            }
            else if (!DebugConsole.Commands.ContainsKey(DebugConsole.Instance.InputParts[1]))
            {
                Debug.LogWarning("'" + DebugConsole.Instance.InputParts[1] + "' command not recognized");
                return false;
            }
            return true;
        }

        public static CommandDescribe CreateCommand()
        {
            return new CommandDescribe();
        }
    }

    public class CommandHelp : ConsoleCommand
    {
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public CommandHelp()
        {
            Command = "help";
            Description = "Lists all the commands on the console and provides a helpful description on how to use them.";
            Help = "Use this command to list all other commands on the console.";

            AddCommandToConsole();
        }

        public override void RunCommand()
        {
            foreach (KeyValuePair<string, ConsoleCommand> command in DebugConsole.Commands)
            {
                Debug.Log(command.Key + " - " + command.Value.Help);
            }
        }

        public static CommandHelp CreateCommand()
        {
            return new CommandHelp();
        }
    }

    public class CommandQuit : ConsoleCommand
    {
        public override string Command { get; protected set; }
        public override string Description { get; protected set; }
        public override string Help { get; protected set; }

        public CommandQuit()
        {
            Command = "quit";
            Description = "Quits the editor and the application.";
            Help = "Use this command to force Unity to quit.";

            AddCommandToConsole();
        }

        public override void RunCommand()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif

            Application.Quit();
        }

        public static CommandQuit CreateCommand()
        {
            return new CommandQuit();
        }
    }
}
