﻿using Console;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CustomCommands : ConsoleCommand
{
    [SerializeField] private string _CommandName;
    [SerializeField] private _CommandType _Type;
    [SerializeField, TextArea] private string _Description;
    [SerializeField, TextArea] private string _Help;
    public UnityEvent EventToRun;

    private enum _CommandType { Simple, Command, Float };

    [HideInInspector] public override string Command { get; protected set; }
    [HideInInspector] public override string Description { get; protected set; }
    [HideInInspector] public override string Help { get; protected set; }

    public override void RunCommand()
    {
        if (EventToRun != null)
        {
            EventToRun.Invoke();
        }
    }

    public void CreateCommand()
    {
        Command = _CommandName.ToLower();
        Description = _Description;
        Help = _Help;

        AddCommandToConsole();
    }

    public override bool ValidateInput()
    {
        switch (_Type)
        {
            case _CommandType.Simple:
                return ValidateSingleCommand();
            case _CommandType.Float:
                return ValidateFloatCommand();
            case _CommandType.Command:
                return ValidateCommandCommand();
        }

        Debug.Log("No validation detected");
        return false;
    }
}